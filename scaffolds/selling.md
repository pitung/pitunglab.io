---
title: {{ title }}
date: {{ date }}
type: {{ layout }}
category: {{ layout }}
tags: [tag, tag, tag]
# replace preview image
preview: 'https://source.unsplash.com/iFSvn82XfGo/700x500'
summary: null
# product data
code: null
old_price: null
price: null
# product feature
overview:
  - list feature
  - of product
  - made to order
shipping:
  - ready to ship
  - deliver as payment
# more product image (url, optional)
images:
  - null
  - null
  - null 
---