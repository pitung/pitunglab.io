---
title: {{ title }}
date: {{ date }}
type: {{ layout }}
category: {{ layout }}
tags: [tag, tag, tag]
# replace preview image
preview: 'https://source.unsplash.com/IQIkl2iGnbw/700x500'
summary: null
# artist
artist: {{ title }}
linkto: '/link/to/artist/site'
# more photo (url, optional)
collection:
  - null
  - null
  - null
---