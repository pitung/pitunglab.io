---
title: cày lại toán nhi đồng
tags: [math, learn, links]
preview: https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTi4XR4PsFk8xA5YIJjBkrpp4_ffrKzz1Bdqa40up16Qz8RBAzPyw
refs:
  - title: art of problems solving
    link: https://artofproblemsolving.com/
    from: aops
date: 2018-11-25 22:02:42
summary: Quên cmn hết cả rồi, giở sách nhi đồng ra xem vã mồ hôi hột. Khó vãi zắm. Phải xem lại để thỉnh thoảng có cớ cốc đầu trẻ con.
---

Khan Academy là nguồn tham khảo thường xuyên. Ngoài ra cũng có vài link khác để tìm hiểu

- [art of problem solving](https://artofproblemsolving.com/) -- các khoá học + sách vở trên này đắt vãi. Nhưng các mục community và resources của nó có nhiều tài liệu tham khảo cũng hay.

---

Phim tài liệu về hệ thống điều khiển vũ khí trên tàu chiến khi chưa có máy tính điện tử, mọi thao tác tính toán (đường đạn) đều dựa trên hệ thống cơ khí.

{% youtube s1i-dnAH9Y4 %}