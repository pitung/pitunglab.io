---
title: "ba cách làm style cho react"
date: 2017-12-21
tags: [react, style, css]
---

Có [3 cách thường zùng](https://blog.logrocket.com/the-best-styling-in-react-tutorial-youve-ever-seen-676f1284b945) để làm style cho react project. Diếu hiểu bọn coder bày ra lắm thế làm điếuji. Càng ngày càng thấy rối. Mỗi dọc hiểu dã thấy chếtcbn mệt roài.

Lần lượt:

- Inline styling
- Styled Components
- CSS Modules

### Inline styling

```javascript
class ToDoApp extends React.Component {
  // ...
  render() {
    return (
      <div style={{ backgroundColor: "#44014C", width: "300px", minHeight: "200px"}}>
        <h2 style={{ padding: "10px 20px", textAlign: "center", color: "white"}}>ToDo</h2>
        <div>
          <Input onChange={this.handleChange} />
          <p>{this.state.error}</p>
          <ToDoList value={this.state.display} />
        </div>
      </div>
    )
  }
}
```

Style Object Variables

```javascript
const TodoComponent = {
  // style in camelCase...
}

const Header = {
  // style in camelCase
}

const ErrorMessage = {
  // style in camelCase
}

class ToDoApp extends React.Component {
  // ...
  render() {
    return (
      <div style={TodoComponent}>
        <h2 style={Header}>ToDo</h2>
        <div>
          <Input onChange={this.handleChange} />
          <p style = {ErrorMessage}>{this.state.error}</p>
          <ToDoList value={this.state.display} />
        </div>
      </div>
    )
  }
}
```


Luwý camelCase chỉ apzụng với tên, không apzụng với játrị. Vz `box-sizing: border-box;` viêt thành `boxSizing: "border-box"`.

### Style component

Muốn zùng fải càidặt trước

```bash
$ npm install --save styled-components
```

rồi import vào project

```bash
import styled from 'styled-components';
```

Tạo file component

```javascript
// normal css syntax
const TodoComponent = styled.div`
    // style in normal css syntax
`;
```

Sửzụng:

```javascript
class ToDoApp extends React.Component {
  // ...
  render() {
    return (
      <TodoComponent> // this component
        <h2>ToDo</h2>
        <div>
          <Input onChange={this.handleChange} />
          <p>{this.state.error}</p>
          <ToDoList value={this.state.display} />
        </div>>
      </TodoComponent>
    )
  }
}
```



### css module

Tạo riêng file css khác, vz `style.css`

```css
.error-message {
    color: red;
    font-size: 16px;
}
```

Rồi import

```javascript
import styles from './styles.css'; // import cái file kia vào

class Message extends React.Component {
     // ... 
    render() {
        return (
            <p className = {styles.error-message}>I am an error message</p> 
        )
    }
}
```
---

Tóm tắt thế đã. Muốn hiểu kỹ hơn fải chịukhó khảo thằng Gúc nữa mới được.

Cánhân ta thích kiểu cuối cùng nhứt! Thấy cove zễ hiểu.
