---
title: forget made in china
tags: [internet, life, business]
refs:
  - title: forget made in china and say hello to made in vietnam
    link: https://m.scmp.com/week-asia/business/article/2157755/forget-made-china-say-hello-made-vietnam/
    from: scmp.com
  - title: fablab Da Nang
    link: https://www.facebook.com/fablabmakerspacedn/
    from: facebook

date: 2018-08-05 13:39:55
summary: "There is a new global buzzphrase — Made in Vietnam"
---

[Bài của Karim Raslan](https://m.scmp.com/week-asia/business/article/2157755/forget-made-china-say-hello-made-vietnam?amp=1) trên South China Moring Post (scmp). Phết phẩy lại tí

Quên "Made in China" đi, thiên hạ đang bắt đầu truyền khẩu "Made in Vietnam".

Một trong 10 smartphone trên thế giới -- bạn có thể đang đọc bài viết này từ nó -- được sản xuất tại Việt Nam. Năm 2017, các sản phẩm của Samsung chiếm một phần tư tổng kim ngạch xuất khẩu 227 tỷ đô của cả nước -- mặt hàng xuất khẩu mạnh, cùng với thép và đồ nội thất.

Quốc gia từng bị chiến tranh tàn phá này đang nhanh chóng định vị chính nó thành một công xưởng mới của thế giới. 

![](https://cdn1.i-scmp.com/sites/default/files/images/methode/2018/08/03/1fab801a-9543-11e8-acb0-2eccab85240c_1320x770_192754.jpg)

Tuy nhiên, người Việt cũng đưa thêm giá trị gia tăng vào sản phẩm của họ theo những cách khác.

Nguyen Ba Hoi là một hiện tượng thú vị ở Đà Nẵng. Hắn không sản xuất smartphone, hay xuất khẩu thép. 

Hắn hướng tới sự sáng tạo.

Từ 2015, Hoi dành thời gian của hắn lập ra "Maker Spaces", trang bị đầy đủ từ máy in 3D, máy cắt laser và tất cả các loại tiện ích công nghệ hiện đại. Ở đó mọi người có thể thử nghiệm ý tưởng, sản xuất thử sản phẩm mới để tung ra thị trường một cách nhanh chóng.

“Khi tôi nghe về công nghệ mới nhất, luôn là sáng kiến của ai đó từ Đức hoặc Anh. Tại sao không phải ai đó đến từ Việt Nam? Đây là điều Maker Spaces có thể làm: cung cấp cho những người trẻ tuổi Việt Nam một cơ hội để tạo nên tên tuổi cho mình”.

![](https://cdn1.i-scmp.com/sites/default/files/images/methode/2018/08/03/822254f8-9543-11e8-acb0-2eccab85240c_1320x770_192754.jpg)

Trên tường của Maker Spaces luôn treo một khẩu hiệu đơn giản: "thiết kế tương lai" (Photo: MaiDuong).

“Tôi yêu triết lý tại Maker Space: Cho dù thất bại, bạn luôn có thể thử lại và thành công. Trước khi thành công, chúng ta phải thất bại rất nhiều”.

---

Thế thôi, còn lại cắt bớt. Những "không gian sáng tạo" như của Hội, sẽ làm được những gì nếu không có một nền sản xuất hùng mạnh hỗ trợ? Sản phẩm thử nghiệm, khi đã được thị trường chấp nhận, lại mang sang TQ đặt làm, hay sao?

Cá nhân tao không tin Việt Nam có thể phát triển kinh tế một cách mạnh mẽ, mà không phải trả giá bằng một cái gì đó. Nó có thể là môi trường, có thể là sự bất ổn xã hội, có thể là sự du nhập của văn hóa ngoại lai...

Để thực sự đưa được "Made in Vietnam" thành cái buzzphrase như tác giả đề cập đầu bài viết, VN sẽ phải trả giá những gì?

Poor me, and my people.