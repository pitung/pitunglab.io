---
title: 'erotic #001'
preview: http://res.cloudinary.com/bidong/image/upload/c_scale,w_700/v1532686497/pitung.gitlab.io/0727/miho-hirano7.jpg
date: 2018-07-27 17:08:17
tags:
  - nsfw
  - art
  - erotic
summary: zự án vẽ vời sắp tới, phân biệt porn vs. erotic
refs:
  - title: tổng hợp art hiện đại
    link: http://www.emptykingdom.com/
    from: emptykingdom
  - title: tổng hợp art cổ đại
    link: http://dolorosa-reveries.blogspot.com/
    from: blogspot
  - title: ladeoji khiêuzâm nghệthuật
    link: https://en.wikipedia.org/wiki/Erotic_art
    from: wikipedia

---

Tra từ điển VN, cả **erotic** và **pornography** đều được dịch là *"khiêu dâm"*. Đéo lạ lắm. 

Đánh art ở VN khoai vãi lọ. Với một chủng tộc "đạo đức giả" từ trong trứng, thì cái đéo gì dính tí tình dục chả bị chúng be rầm lên là "khiêu dâm", "đồi trụy". Nhẹ thì cũng bị chụp cho cái mũ "phạm thuần phong mỹ tục".

Trích wikipedia:

"A key distinction, some have argued, is that **pornography's** *objective is the graphic depiction of sexually explicit scenes*, while **erotica** *"seeks to tell a story that involves sexual themes"* that include a more plausible depiction of human sexuality than in pornography."

Đọc đéo hiểu bảo thằng Gúc nó dịch cho.

Tao đang bắt đầu những phác thảo đầu tiên của dự án **erotic aodai**. Dự án dành riêng cho bọn trung niên đạo mạo, tay xóc lọ gầm bàn, mồm rêu rao thuần phong mĩ tục. 

Đéobiết bao giờ hoàn thiện. Hãy đợi đấy! dcmcm.

![](https://res.cloudinary.com/bidong/image/upload/v1532691159/pitung.gitlab.io/0727/Image023.jpg)

### một số tham khảo

![](https://res.cloudinary.com/bidong/image/upload/v1532687936/pitung.gitlab.io/0727/franz-von-bayros-retro-sensualita.jpg)
[Franz von Bayros](https://en.wikipedia.org/wiki/Franz_von_Bayros)

![](http://res.cloudinary.com/bidong/image/upload/v1532686014/pitung.gitlab.io/0727/Leo_2BImpekoven_2B_25281873-1943_2529.jpg)  
http://dolorosa-reveries.blogspot.com/2018/01/leo-impekoven-1873-1943-adam-and-eve.html

![](https://res.cloudinary.com/bidong/image/upload/v1532686353/pitung.gitlab.io/0727/kaethe-butcher4.jpg)  
http://www.emptykingdom.com/featured/kaethe-butcher/

![](https://res.cloudinary.com/bidong/image/upload/v1532686497/pitung.gitlab.io/0727/miho-hirano7.jpg)
http://mihohirano.strikingly.com/

![](https://res.cloudinary.com/bidong/image/upload/v1532686691/pitung.gitlab.io/0727/Hiroko-Shiina1.jpg)
http://c7-shiina.tumblr.com/

![](https://res.cloudinary.com/bidong/image/upload/v1532686869/pitung.gitlab.io/0727/Andi-Soto9.jpg)
http://www.andisoto.com/

