---
title: how is schooling in Vietnam
tags: [life, copy, education]
refs:
  - title: thảo luận trên Quora
    link: https://www.quora.com/How-is-schooling-in-Vietnam
    from: Shirley Featherquill
date: 2018-08-27 21:35:42
summary: Well, honest I should be, the word is “hellish” — Tao đồng ý với ý kiến này. Không bởi vì lượng kiến thức bọn học trò phải tiếp thu, mà vì rất nhiều trong số đó được xây nên từ dối trá.
---

Một ý kiến về giáo dục VN trên Quora, lưu lại đọc dần, dài vãi. 

https://www.quora.com/How-is-schooling-in-Vietnam

![](https://imgix.bustle.com/uploads/image/2018/8/14/7f4ffefb-470c-4acf-b702-add8618a51dd-fotolia_80960190_subscription_monthly_m.jpg)

Trước jờ vẫn cứ cảm thấy VN có rất nhiều thứ quái gở, nhưng không thể gọi tên hay lý jải, cũng không đủ trình để lý luận về những thứ đó. Mỗi nền văn hoá có kiểu phát triển riêng. Văn hoá VN mấy chục năm nay phát triển dựa trên nền tảng sùng bái cá nhân và nịnh bợ. Không có chỗ cho những tư tưởng ngược dòng. Có thể nói cuộc sống cá nhân của một người lệ thuộc hoàn toàn vào tư tưởng của người đó. Thiên hạ đánh giá anh không bởi năng lực, mà còn cả tư tưởng của anh.

Gần đây thấy tình hình có vẻ khá khẩm hơn. Những ý kiến phản biện tuy vẫn bị ném đá, nhưng không còn ảnh hưởng trực tiếp tới cuộc sống của người nêu ý kiến. Cũng có thể nói tư tưởng và năng lực phần nào đã được tách biệt. Được đánh giá trên hai kênh khác nhau.

https://vnexpress.net/tin-tuc/cong-dong/y-kien-cua-toi/khong-nen-de-tre-hoc-doc-tieng-viet-bang-cach-danh-van-3800549.html
