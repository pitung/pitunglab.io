---
title: highlighting text problem
date: 2018-07-26 17:42:13
tags:
  - css
  - style
  - webdev
refs:
  - title: multi line padding text
    link: https://css-tricks.com/multi-line-padded-text/
    from: css-tricks
summary: "vấn đề highlighting riêng phần text của một khối chữ khi trình bày trang web"
---

Làm lại blog theo tạng card-based đụng fải vấn đề trình bày chữ trên tiêu đề.

Content card có hình nền, nên khối chữ tao chỉ muốn highlight fần text. Ngắt dòng không kiểm soát được đã đành, đoạn highlight chỉ dừng lại đúng chỗ ngắt dòng trên nhìn rất khó chịu. Đoạn bắt đầu dòng dưới cũng thế. Các thông số `padding` và `margin` chỉ áp dụng cho toàn bộ khối chữ.

Zĩnhiên jảifáp đơnjản nhất là bỏ qua và ápzụng cách trìnhbày khác. Trường hợp không thể bỏ qua *(bị khứa úp sọt chả hạn)*, [mời nghiêncứu một số jảifáp được tổng hợp ở đây](https://css-tricks.com/multi-line-padded-text/).

Làm xong nom nó như nài:

![](http://res.cloudinary.com/bidong/image/upload/c_scale,w_1000/v1532602786/pitung.gitlab.io/Zalo_ScreenShot_26_7_2018_1058880.png)

-- Có cách khác zễzàng hơn?  
-- Chắc là có. Ở đây tao chỉ ápzụng cách mà tao hiểu thôi.

### html (lược bớt cho đỡ rối mắt)

```html
<div class="grid">
    <div class="grid_item card">
        <div class="card_header">
            <a href="#">
                <div class="card_date"><span>date time</span></div>
                <h2><span>card title</span></h2>
            </a>
        ...
</div>

```

### css (stylus)

```stylus
.card_header
    a
        overflow: hidden
        font-weight: 700
        display: block
        min-height: 180px
        &:hover
            color: #666
    .card_date
        font-size: 80%
        color: #555
        span
            display: inline-block
            background: #000
            padding: 10px
    h2
        display: inline
        font-family: fh
        font-size: unit*2.4
        color: #fff
        line-height: 125%
        span
            box-decoration-break: clone
            -webkit-box-decoration-break: clone
            -ms-box-decoration-break: clone
            -o-box-decoration-break: clone
            background: #000
            box-shadow: 10px 0 0#000, -10px 0 0 #000
            padding: 0 10px 0 0
            margin-left: 10px

```

Lằng nhằng fết.