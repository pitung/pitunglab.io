---
title: 'cần bộ tool khác cho webshop'
tags: [hexo, shop, webdev]
date: 2018-09-06 15:52:41
summary: Tính linh động của static site không đủ để làm webshop. Mât toi cả tuần trăn trở... huhu
preview: https://i.guim.co.uk/img/media/085daf0d2fb04c37f70df18e8a32160ca4050e1a/0_149_5503_3301/master/5503.jpg?width=700&quality=85&auto=format&fit=max&s=654f4df0a6d88ce4f47848a10249f1cd
---

Mải mê dev cái interface mới cho hexo với ý định làm ra một trang bán hàng khả dĩ có thể dùng được. Làm xong nhận thấy mấy vấn đề khó giải quyết. Có lẽ nên kiếm bộ tool khác.

Vấn đề đầu tiên là Không thể list bài tạng random. Đặc trưng của static site là không dễ biến hóa như dynamic site. Nếu list sản phẩm trên webshop theo thứ tự thời gian, các mặt hàng đầu tiên post lên hầu như sẽ không có cơ hội được nhìn thấy. Càng nhiều sp, vấn đề này càng nghiêm trọng.

Vấn đề thứ hai là với giao diện flipcard. Trình bày tạng cardbase là ok, nhưng khi áp dụng hiệu ứng lật đi lật lại nhận thấy tính tương thích của nó rất kém. Chạy trên firefox và blackberry bị lỗi. Thiên hạ không mua hàng bởi các hiệu ứng cầu kỳ trên web. Không áp dụng được cho webshop nhưng dùng làm *siêc* cũng được. Tạm thời giữ lại tham khảo dù không xem được trên firefox.

![](https://res.cloudinary.com/bidong/image/upload/v1536224795/pitung.gitlab.io/0906/shopshiec.png)

Mất cả tuần cày cuốc, cuối cùng không dùng được. Quá tệ.

### Giải pháp???

Với vấn đề thứ nhất. Vuejs có vẻ là lựa chọn hợp lý nếu vẫn muốn chạy theo hướng serverless. Giờ mà cày lại WP không biết đến đời nào mới xong. Kiến thức PHP về mo từ đời nảo đời nào cbn rồi. Vấn đề thứ hai dễ giải quyết hơn vì nó không nằm ở công cụ. Vứt mẹ flipping đi là xong.