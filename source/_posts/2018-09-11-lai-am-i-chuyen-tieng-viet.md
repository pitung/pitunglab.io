---
title: lại ầm ĩ chuyện tiếng việt
tags: [write, life]
date: 2018-09-11 13:29:11
summary: Thiên hạ lại ầm ĩ phản đối dạy tiếng Việt theo cách mới của gs Hồ Ngọc Đại. Thiết nghĩ thôi cứ kệ mẹ bọn chúng. Để một bọn học thành người văn minh, còn bọn kia học thành người Việt là được.
preview: https://res.cloudinary.com/bidong/image/upload/v1536649203/pitung.gitlab.io/0906/2018-09-11_135931.png
---

Dân tình lại ầm ĩ chuyện dạy và học tiếng Việt. Lúc đéo nào chả vậy. Mấy chục năm nay, chỉ cần hóng hớt thấy có gì khác thường chúng chả be be rầm rĩ lên như thế. Thói tư duy sùng bái cá nhân và tự hào dân tộc bị nhồi nhét mấy chục năm, nào có dễ bỏ.

![](https://res.cloudinary.com/bidong/image/upload/v1536649203/pitung.gitlab.io/0906/2018-09-11_135931.png)

Cũng định biên tí chút, nhưng cảm thấy chán nản quá. Thôi kệ mẹ. Chả liêncan đéo gì đến miếng cơm manh áo, dăm ba câu bàn góp tác dụng đéo gì. 

Thế nhưng quả thật rất muốn nhắn tới giáo sư, rằng chả việc gì phải trăn trở. Đứa nào muốn học nên người văn minh thì đã học. Đứa nào phản đối thì kệ cụ bọn nó gặm nhấm mấy thứ giẻ rách nhai đi nhai lại suốt mấy chục năm nay. Cứ để chúng là người việt như chúng tự hào.

Thế giới vốn bao la, có đủ chỗ cho tất cả.