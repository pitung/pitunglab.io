---
title: tiếp tục cày hugo
tags: [webdev, hugo, study]
date: 2018-10-06
summary: dùng hugo một thời gian, bắt đầu hiểu cấu tạo và khái niệm của nó, thấy những e ngại trước giờ về sự phức tạp của nó hoàn toàn vô lý
preview: http://www.hugoandmarie.com/wp-content/uploads/2018/03/hugomarie_mariohugo_thenewyorktimesmagazine_dissesctingahumanbody01-1-800x582.jpg
---

Hugo thực sự là công cụ mạnh mẽ để đánh site tĩnh. Tốc độ nhanh, cài đặt đơn jản, đéo cần quan tâm đám thư viện fụthuộc. Nhõn file exe, tải về ném một xó rồi cứ thế chạy. Thiên hạ khen đầy, Gúc phat ra cả đống, khỏi viết lại.

Kinh nghiệm

- [hugo đocs]() chỉ hợp dùng khi cài đặt và khi đã dùng quen, đoạn lớ ngớ ở giữa cần tìm tuts khác chi tiết hơn.
- [cấu tạo hệ thống thư mục của hugo]() là bước đầu tiên cần nắm chắc
- các tuts dùng `gulp` để dev thêm là dành cho các phiên bản cũ, từ 0.43 nên [dùng hugo pipe]() thay thế
- cân nhắc dùng [amber]() làm template
- hạn chế customize front matter cho đến khi hiểu và dùng thạo `archetype`.
- không dùng shortcode trong nội dung, trừ khi nội dung đó chỉ dùng trong phạm vi hugo site.
- nên config để hugo đọc thông tin date time tự động theo file (create, last modify) thay vì nhập thủ công.
- content tạng blog thông thường thì front matter chỉ cần title, categories, tags, summary là đủ. Cùng lắm thêm preview image

some link

- https://www.client9.com/using-font-awesome-icons-in-hugo/
- https://regisphilibert.com/blog/2018/01/hugo-page-resources-and-how-to-use-them/
- https://medium.com/@timothefaudot/minimalist-material-design-template-for-hugo-using-amber-53e30f33e468
- https://novelist.xyz/tech/hugo-data-files/
https://medium.com/@jeffmcmorris/tips-and-tricks-for-building-a-theme-in-hugo-4806bdd747d7

liên quan

caddy là webserver dễ cài đặt và dễ dùng nhất từng biết [theo ý kiến của một người dùng](https://novelist.xyz/tech/caddy-webserver/), được viết bằng Go language. Mới cài đặt và dùng thử, chưa thấy lợi ích gì lớn. Có lẽ chỉ dùng cho bọn thích tự làm hóting ở nhà.

Đang thắc mắc ssg (aka static site generator) nào cũng có sẵn server để chạy thử (xem trước). netlify, github, gitlab cũng có thể chạy deploy, bản thân SSG cũng build ra một cái public folder, đéo hiểu sao bọn nó vẫn phải deploy lần nữa từ cái puplic folder ý lên host. Cái public folder ý rõ ràng chỉ cần copy rồi ném thẳng vào host cho nó chạy, deploy lần nữa làm đéo gì nhỉ? https://dev.to/hugo__df/deployment-options-netlify--dokku-on-digitalocean-vs-nowsh-github-pages-heroku-and-aws-4cab

khác biệt giữa *framework* với *library* là gì? [thảo luận trên dev.to](https://dev.to/ben/whats-the-difference-between-a-library-and-a-framework-3blo). Đại khái có thể hình dung lib là môt món công cụ nào đó với một vài tính năng cụ thể (có thể dùng cho nhiều máy), còn frmw là cỗ máy gia công. Vậy các SSG đều là framework.







