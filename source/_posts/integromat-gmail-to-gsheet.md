---
title: "integromat - gmail to gsheet"
date: 2017-12-23 14:42:47
tags: [integromat, automation, code]
---

Ta zựng một qwitrình theozõi việc làm khuôn kẽm. Dòuvào (input) là email, dòura (output) là file google sheet dể tiện tổnghợp sau này.

Lyzo zùng email vì dó là côngcụ jảntiện và nhanhchóng nhât có trong tay. Hòunhư tablet/smartphone nào cũng có.

### thenao ghichep?

Nộizung ghi bằng email theo một cúfap riêng, rồi gửi vào dịachỉ gmail xáclập sẵn `(motbidong@gmail.com)`. 

```text
// tiêudề (title)
tzkhuon [date] // ngày tháng có thể không ghi cũng dược.

// nội zung email (mỗi thongtin một zòng)
mt đế lịch tp ep kh côngty cổfần khôngđăngký w 21 h 24
mt khánh lịch tp thuc kh một kháchàng naodo w 28 h 34
mt khuôn vàng mã tp vm  kh một thằng khách khác w 58 h 33
mt đế lịch tp 3d kh thằng khác nữa w 10 h 15
```

Daikhai cothe hiểu như sau:

- **mt** -- mô tả
- **tp** -- loại khuôn
- **kh** -- khách hàng
- **w** -- chiều ngang
- **h** -- chiều zọc

Cúfáp tựtạo sao cũng dược, miễn là zễ nhớ, zễ viêt.

### thenao regex?

Regex hay regexp là tạng côngthức zòtìm, dể truy kếtqwả trong một mớ zữliệu tạpnham. Bọn VN kêu bằng “biểuthức chínhqwi”. Khôngsai, nhưng nghe dầndộn hãmlìn.

Trướchêt fải ghilại cái [regex](https://vi.wikipedia.org/wiki/Bi%E1%BB%83u_th%E1%BB%A9c_ch%C3%ADnh_quy) này:

```regex
mt\s*(?<mt>.*?)\s*tp\s*(?<tp>.*?)\s*kh\s*(?<kh>.*?)\s*w\s*(?<w>.*?)\s*h\s*(?<h>.*)\r*
```

Doạn này zùng dể lọc nộizung email trên thành các nhóm thongtin cầnthiêt. Copy doạn trên vào [regex101](https://regex101.com/) xem nó jảithich.

[![](http://res.cloudinary.com/bidong/image/upload/c_scale,w_650/v1514018296/2017-12-23_153613_pzclzo.png)](http://res.cloudinary.com/bidong/image/upload/v1514018296/2017-12-23_153613_pzclzo.png)

### thenao integromat?

Một **[integromat](https://www.integromat.com/) scenario** sẽ theo zõi dịachỉ email dã nói trênkia. Nếu có email fùhợp diềukiện (from *my email* VÀ có chữ `tzkhuon` ở subject), nó sẽ lấy nộizung email dó dể ghi vào file googlesheet.

Tạo scenario theo sơdồ zưới dây

![](http://res.cloudinary.com/bidong/image/upload/v1514014476/Gmail_content_to_Google_sheet_Integromat_kra37p.png)

**Input: Gmail > Watch Email** -- Folder: All mail; Query: `subject: tzkhuon`. [fig.1](http://res.cloudinary.com/bidong/image/upload/v1514017587/Gmail_content_to_Google_sheet_Integromat-in_yz2xkf.png)

**Process: Text Parser > Match Pattern** -- Pattern: copy doạn regex trênkia vào; Text: chọn *text content* của bước 1. [fig.2](http://res.cloudinary.com/bidong/image/upload/v1514017588/Gmail_content_to_Google_sheet_Integromat-prc_clxo50.png)

**Output: Google Sheets > Add a Row** -- Chọn ghi vào các mục tươngứng. Luwý cột DATE lấy ngày gửi email (bước 1). [fig.3](http://res.cloudinary.com/bidong/image/upload/v1514017587/Gmail_content_to_Google_sheet_Integromat-out_ecfupu.png)

Chạy thử thấy ngon rồi.

---

Luwý: côngthức này cothe apzụng nhiều việc khác. Vizu theozõi chitiêu, ghichep ýtưởng, collect thongtin etc. Có [rât nhiều zịchvụ](https://www.integromat.com/en/kb/packages.html) cothe khaithac.