---
title: lại muốn sửa giao diện blog
date: 2018-07-26 10:35:31
tags:
  - design
  - webdev
  - style
  - idea

refs:
  - title: building static site with nunjucks
    link: https://www.smashingmagazine.com/2018/03/static-site-with-nunjucks/
    from: smashingmagazine.com
  - title: pros and cons of card-based design
    link: https://medialoot.com/blog/what-is-card-based-design-and-should-you-be-using-it/
    from: medialoot.com

---

Dùng [hexo](https://hexo.io/) cho dù quen rồi cũng vẫn khó chịu với cái [ejs templating language](http://ejs.co/) mặc định của nó. Tao ưa [liquid](https://shopify.github.io/liquid/) của jekyll/shopify hơn.

Nay xem [nunjucks](https://mozilla.github.io/nunjucks/) thấy hay, ý định ápzụng.

Khởi tạo site mới, sửa `package.json`, thay `hexo-renderer-ejs` bằng `hexo-renderer-nunjucks`, chạy `npm install` báo lỗi. Xem trên [hexo plugins](https://hexo.io/plugins/) cũng đéo thấy cái nào tên là nunjucks, chắc không hỗ trợ. Thôi bỏ qua.

---

Nguyên nhân của đoạn mò mẫm trên kia là vì [một bài viết trên smashingmagazine](https://www.smashingmagazine.com/2018/03/static-site-with-nunjucks/)

Thử nunjucks với hexo không được, nhưng nhặt được quả design ưng mắt

![](http://res.cloudinary.com/bidong/image/upload/c_scale,w_1000/v1532576994/pitung.gitlab.io/Zalo_ScreenShot_26_7_2018_332146.png)

Ý định làm lại cái jaoziện jống thế.

Đánh site [tạng card based](https://medialoot.com/blog/what-is-card-based-design-and-should-you-be-using-it/) tao ưng. Nom trên smartphone hay tablet hay computer đều ổn.

Này là jaoziện lôc hiệntại.  

![](http://res.cloudinary.com/bidong/image/upload/c_scale,w_1000/v1532577461/pitung.gitlab.io/Zalo_ScreenShot_26_7_2018_357328.png)

Chỉ là con lôc vui tay đánh ra làm chỗ làm nhảm. Ý định ban đầu chỉ monospace text font, kiểu máy chũ xưa, kếtcấu jảnlược đủ zùng. Xem lâu thấy nhàm.

Tiện cày cho khách, tiện tay cày luôn cho mình cái jaoziện mới. Cũng là thỉnh thoảng nhắc lại tí cho đỡ quên.

---

### bonus :: pros and cons of card-based design

#### Pros of Card-Based Design:

**Adaptive <span style=color:salmon>-- phù hợp nhiều loại fương tiện</span>**

Having your content contained within individual cards makes them easy to re-arrange and re-order. This is particularly important when it comes to responsive design for various devices and screen sizes. For this reason, cards make perfect sense to designers.

**Scan-ability <span style=color:salmon>-- zễ theo zõi</span>**

Card-based layouts present your visitors with lots of short bursts of information, that are easy to scan and find what they need. Where as traditional layouts may dedicate their entire homepage to saying "hello" and telling you about a company, cards can show snippets of content from all across the site, meaning that visitors are more likely to find what they are looking for on the very first page they land at.

**Bite-Sized Chunks of Information <span style=color:salmon>-- ngắn gọn, súc tích</span>**

Cards are good for communicating quick stories, things which don't necessarily require a whole paragraph of text, let alone their own dedicated page, but are important nonetheless. Much like on Social Media, these shorter straight to the point snippets of information are easier to digest and more likely to actually be read by your visitors/followers. And you can always provide a 'Read More' link if you do have more information to provide for those who are particularly interested.

**Social Media Integration <span style=color:salmon>-- dễ tích hợp</span>**

As briefly mentioned in the last point, cards have a lot in common with social media. Tweets, Facebook posts, Instagram photos etc. all fit the criteria of cards. So they are ideal for integrating into your website.

#### Cons of Card-Based Design:

**Less Consistency <span style=color:salmon>-- khó nhất quán</span>**

Unless all of your widgets visually look the same (which would be a bad practice in itself), a card-based design may require you to use a larger range of colours, images, fonts or other variables to distinguish between cards. Less consistency in this fashion can make your layout look disorganised.

**Lack of Traditional Hierarchy <span style=color:salmon>-- không hợp truyền thống</span>**

Although there are methods to bring attention to more important cards, the lack of a traditional hierarchy in which more important content is displayed first, can have a negative effect. Especially for visitors to a website who are looking for a specific piece of information, throwing a grid of seemingly unorganised snippets of content at them may leave them confused and not knowing where to look.

**Not Suitable for Some Websites <span style=color:salmon;>-- không fải thằng nào cũng ưa</span>**

Card-based design may not be suitable for smaller projects or projects that are primarily information based. For a website with the sole purpose of proving detailed information, a card-based design would be a step in the wrong direction. It would be counterintuitive to break a design up into multiple isolated segments, when all your visitor really wants to do is read the information you're offering. Likewise, for smaller projects which simply don't have a lot of content, switching to a card-based design will only confuse your visitors. Smaller websites that have less content tend to be easy to navigate anyway, so why fix what isn't broken?