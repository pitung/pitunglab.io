---
title: html span tag in hexo markdown post
date: 2018-07-28 11:49:15
tags: [code, hexo, style, problem]
summary: lỗi hexo render, hay gì?
---

Trình bày bài viết bằng markdown (trong hexo, theo tao biết đến thời điểm này) có thể format text theo các dạng **bold**, *italic*,  ~~crossline~~, `code`, [link](#) (không dùng được underline???). 

List, blockquote, table xét sau.

Tình cờ tao muốn vài chữ **<span style=color:blue;>hiển thị màu khác</span>**. Nảy ý định zùng thêm `<span>` tag.

Đánh tạng html chuẩn đéo ăn thua

```html
text thường <span style="color: red;">text đỏ</span> text thường
```

Phải đánh tạng này mới được *(bỏ ngoặc kép, không dấu cách)*

```html
text thường <span style=color:red;>text đỏ</span> text thường
```

Đéo biết cái này là lỗi render của hexo hay jì.  
Browser: chrome, firefox