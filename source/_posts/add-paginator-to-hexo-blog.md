---
title: "paginator added"
date: 2017-12-20 10:36:47
tags: [hexo, paginator, code]
---

Zựtính cứ keme cho conlôc này list toànbộ post. Sau tính lại, thấy nhìn hơi zàizòng.

```html
<hr class="section_divide">
<% if (page.total > 1){ %>
    <nav id="page-nav">
        <% var prev_text = "&laquo; ";var next_text = " &raquo;"%>
        <%- paginator({
        prev_text: prev_text,
        next_text: next_text
        }) %>
    </nav>
<% } %>
```

Thích chitiêt nữa thì độn thêm cái này:

```html
<p>Page <%= page.current %> in total <%= page.total %> pages</p>
```