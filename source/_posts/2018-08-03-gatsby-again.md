---
title: "gatsby again?"
tags:
  - gatsbyjs
  - webdev
  - airtable
refs:
  - title: building blog with airtable and gatsby
    link: https://blog.airtable.com/build-your-own-custom-blog-cms-with-airtable-and-gatsbyjs/
    from: airtable
  - title: gatsby starter e-commerce
    link: https://parmsang.github.io/gatsby-starter-ecommerce/
    from: gatsby
  - title: gatsby documentation
    link: https://www.gatsbyjs.org/docs/
    from: gatsby
date: 2018-08-03 12:17:16
summary: "chuẩn bị cho quy trình tự động hóa thông tin"
---

Cuối năm ngoái {% post_link playing-around-gatsbyjs "đã dùng thử" %}, nhưng giờ quên tuột cbn hết rồi.

Tao đã dùng [Slack](https://slack.com/) và [Integromat](https://www.integromat.com/) cho {% post_link tong-ket-integromat "vài thử nghiệm nho nhỏ" %}, tự động collect message từ slack channel rồi ghi thành data table *(google sheet hoặc airtable)*. Mấy tháng qua dùng phát nghiện hehe.

Nay ý định thử đào bới sâu hơn. Bước đầu đặt mục tiêu nhỏ thôi:

- cài đặt và chạy được gatsby trên local computer
- dựng ra được cái blog dùng [airtable](https://airtable.com/) làm nguồn cấp dữ liệu.
- deploy it on netlify

Rất hào hứng, bởi vì tao mò được trên Airtable blog [bài hướng dẫn (có vẻ) rất chi tiết](https://blog.airtable.com/build-your-own-custom-blog-cms-with-airtable-and-gatsbyjs/). Trên gatsby có sẵn nhiều [starter theme](https://www.gatsbyjs.org/docs/gatsby-starters/). Ý định trước hết là cứ copy-paste theo hướng dẫn cho nhanh.

### install

Cài đặt gatsby bằng `yarn` thấy ok nhưng không chạy. Lý do chưa biết. Vẫn phải dùng `npm`

```bash
npm install --global gatsby-cli
```
Cài xong thử tạo site mới theo hướng dẫn

```bash
gatsby new airtable-blog https://github.com/davad/gatsby-hampton-theme
```
Teminal **chạy ra một đống lỗi** *(như mọi bận)*. Kiểm tra các phiên bản node & npm đều ok, `npm uninstall -g yarn`, [dơnload & install yarn for windows](https://yarnpkg.com/en/docs/install#windows-stable) cũng không ăn thua. Thử thay **Gitbash** bằng **Window Powershell**, đổi starter theme thành https://github.com/gatsbyjs/gatsby-starter-blog thấy ok.

Lưy ý mấy thứ này mỗi lần install hoặc remove chạy rất lâu (quá nhiều file cần tải/đọc/ghi?). Mất khoảng 15p mới cài xong. Thử `gatsby develop` rồi vào `localhost:8000` thấy ok rồi *(cũng phải mất một lúc lâu mới khởi động xong)*

### airtable

- khởi tạo table, fill vài record ví dụ
- lấy table id và api key

**Tao tắc tị ở chỗ này**. Copy - paste code mẫu không ổn, vì code theme khác nhau. Đọc từ đầu để lấy cơ bản quá mất thì giờ.

**Vấn đề ở chỗ nào?** Phiên bản windows quá cũ *(đang zùng win7)*? các thư viện dependency không đầy đủ? Môi trường chạy không phù hợp? Cả đống câu hỏi nảy ra mà vẫn mù mờ đéo biết giải quyết thế nào. Chưa có ý định cài lại hoặc nâng cấp windows. 

Tạm dừng lại đây đã. 