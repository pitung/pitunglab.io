---
title: thiết kế lại từ đầu?
date: 2018-06-21 08:54:49
tags:
  - design
  - webdev
refs:
  - title: trang thử nghiệm của dvagribank
    link: http://dvagribank.netlify.com/
    from: gitlab, netlify
  - title: ladeoji đơn vị vh, vw, vmin, vmax?
    link: https://web-design-weekly.com/2014/11/18/viewport-units-vw-vh-vmin-vmax/
    from: web-design-weekly.com
  - title: complete guideline for css-grid
    link: https://css-tricks.com/snippets/css/complete-guide-grid/
    from: css-tricks.com
  - title: fullscreen slideshow
    link: https://tympanus.net/codrops/2012/01/02/fullscreen-background-image-slideshow-with-css3/
    from: tympanus

---

Ngại mãi không được. Nay jở site cũ ra xem. Ý định làm lại.

Ông ý [đây](https://github.com/dvagribank/dvagribank.github.io)

Ngày đó, [Jekyll](https://github.com/jekyll/jekyll) là côngcụ quen tay nhất sau Wordpress. Tao không chọn WP vì muốn _hạn chế khả năng tùy biến_ của khách. Một website với nhiều khả năng tùy biến, thực sự là thứ mang lại rắc rối. Mặc zù yêu quý khách hàng rất, thế nhưng tao không ý định mất thì jờ chạy theo chùi đít cho chúng.

Thời jan chứng minh lựa chọn của tao là chính xác.

## Lý do làm lại

Thời điểm dựng ông ý, kiến thức webdev của tao rất hạnchế. Thời gian _mày mò thử - sai_ quá nhiều. Đầu óc như thùng nước gạo. Nản rất.

Mấy tháng sau đó, tao đâm đầu vào một hobby khác. Âu cũng là fản ứng của não bộ khi bị quá tải.

Thời jan ông ý hoạt động, thỉnh thoảng tao có đảo qua xem. Ý định sửachữa không chỉ nảy ra một lần. Nhưng ám ảnh trước đây vẫn còn quá lớn. Thực sự ngại.

Nay khách đưa yêu cầu nâng cấp, nói chung cũng là dịp để sửa chữa những sai sót trước đó. Bắt tay vào làm thôi.

## làm lại như nào?

- Jảitán Jekyll. Training khách hàng zùng hexo zễ hơn.
- Zùng [netlify](https://www.netlify.com/) và [cloudinary](https://cloudinary.com/)
- Ápzụng [css grid](https://css-tricks.com/snippets/css/complete-guide-grid/) làm layout
- ~~Zùng [glidejs](https://glidejs.com/) làm slider~~ [cái này hay hơn](https://tympanus.net/codrops/2012/01/02/fullscreen-background-image-slideshow-with-css3/)
- Zùng [viewport unit](https://web-design-weekly.com/2014/11/18/viewport-units-vw-vh-vmin-vmax/) làm style cho homepage (fullscreen)
- Zùng gitlab thay github

---

## setting up

Có kinhnghiệm từ trước, zựng một hexo site rồi zùng netlify deploy nhanh thôi. Ghi lại cũng không thừa.

### Computer: 
- `cd c:/www/OneDrive` - tao muốn zùng OneDrive để backup
- `hexo init dvagribank.gitlab.io` - khởi tạo site

### Gitlab
- tạo group **dvagribank** - đỡ lẫn lộn project khác
- tạo repository **dvagribank.gitlab.io**

### Computer:
- `cd c:/www/OneDrive/dvagribank.gitlab.io`
- `git init`
- `git remote add origin git@gitlab.com:dvagribank/dvagribank.gitlab.io.git`
- `git add .`
- `git commit -m "khoi dong"`
- `git push -u origin master`

### Netlify
- đăng nhập, thêm project mới, chọn repo từ gitlab
- netlify tự động detect và cài đặt thông số mặc định
- đợi deploy (mất khoảng 1 phút)
- sửa domain name (http://dvagribank.netlify.com)

Xong rồi đấy!!!