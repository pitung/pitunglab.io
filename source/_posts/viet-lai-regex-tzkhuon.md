---
title: viêt lại regex tzkhuon
date: 2017-12-25
tags: [regex, automation]
---

Cach ghichep theozoi làm khuôn kẽm [bài trước](https://motbidong.netlify.com/2017/12/23/integromat-gmail-to-gsheet/) hơi zaizong. Chạy vẫn ổn, nhưng viet email theo kufap dấy hơi fiền.

Dayla cach viet mới

```text
epn 30 4 cty cofan khongdangky
thk 9 11 cty tnhh tm & dautu chuadangky
vma 58 33 cty tnhh thuong mai sedangky
cnc 12 15 xuong san xuat suytdangky
```

Có 4 loại khuôn: **epn** — ep nhũ, **thk** — khuôn thuk nổi, **vma** — khuôn vàngmã & **cnc** — khuôn 3d.

4 nhóm tin viet theo thứ tự: 

`[loại_khuôn] [ngang(w)] [zọc(h)] [khách hàng]`.

Regex

```regex
(?<type>epn|thk|vma|cnc)\s*(?<w>\d*)\s*(?<h>\d*)\s*(?<kh>.*\r*)\r*
```

Luwý nhóm `<type>...` cothe viêt thành `(?<type>.\w*)` dể lấy nhóm text bâtky, tuy hơi fiềnfức khi dưa vào bảng theozoi, nhưng **khônglo thongtin bị bỏsot** nếu sai kufap.
