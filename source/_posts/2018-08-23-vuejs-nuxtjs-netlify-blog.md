---
title: 'vuejs, nuxtjs, netlify blog'
tags: [vue, nuxt, webdev]
refs:
  - title: introduce to bael blog theme
    link: https://dev.to/jasperketone/top-10-reasons-to-use-bael-a-netlifycms-blog-theme-4i6
    from: jake - dev.to
date: 2018-08-23 22:00:48
summary: Định không thèm ngó ngàng gì đến mấy thứ lằng nhằng rắc rối này nữa, nhưng thấy cái theme có vẻ hay. Thuổng về nghiên cứu dần.
---

Tạm [quăng lên netlify](https://naothivui.netlify.com/). Ý đồ là để ít nữa không việc jì làm thì lôi ra mổ xẻ nghiên cứu. E rằng khó, bởi vì lúc nào tao cũng có xu hướng làm những việc không quan trọng trước. Rất nhảm.

Có mấy lý do tao thích cái theme này:

- Vừa đủ tính năng, không quá rườm rà
- Giao diện thô sơ
- Vuejs, Nuxtjs là mấy thứ đang cần tìm hiểu

Clone cái git repo về đã thấy nó gần bốn chục MB. `npm install` chạy rất lâu ~~và dường như đang có xu hướng tắc tị. Không biết cài đặt xong lên bao nhiêu đây~~ và cuối cùng cũng xong. Tổng dung lượng app hiện đang gần 300MB. Càng hiện đại càng thấy rườm rà. Xưa cả gói phần mềm có vài chục MB làm được bao việc. Nay tải cả GB về nhiều khi cũng chỉ để gõ vài cái text.

`npm run dev` chạy ok rồi. Thằng này dùng port 3000. 

Lướt qua một lượt file/folder, thấy toàn file .json. Hơi ngạc nhiên. Không fải nó quảng cáo dùng markdown sao? Login vào trang admin (`localhost:3000/admin`) thấy nó đòi liên kết identity gì đó với netlify. Bật netlify identity lên thì không bị báo lỗi nữa, nhưng cái trang quản trị vẫn không chạy ra được. Điếu biết tại sao.

Nản lắm rồi. Mấy cái app chó má này.