---
title: google maps in hexo site
tags: [webdev, hexo, map]
refs:
  - title: hexo-tag-googlemaps plugin
    link: https://github.com/the-simian/hexo-tag-googlemaps
    from: hexo.io
date: 2018-07-31 15:35:54
summary: thêm bản đồ vào trang contact
---

Không có cũng không sao. Chỉ là tiện tay làm cho vui.

```bash
npm install --save hexo-tags-googlemaps
```

Thêm vào `_config.yml`

```yaml
google_maps_api_key: //your google api key here
```

[API là gì?](http://genk.vn/kham-pha/giai-ngo-ve-api-vi-sao-noi-api-co-y-nghia-song-con-voi-ca-the-gioi-dien-toan-20160403144419703.chn) Làm sao để có nó? Lúc nào có hứng viết sau.

Trong hexo page (ví dụ `/about/index.md`)

```text
{% googlemaps 21.047 105.795 4 100% 450px %}
  I live here :D, 21.047699, 105.795275, https://res.cloudinary.com/bidong/image/upload/v1533111267/pitung.gitlab.io/wellcome.png
{% endgooglemaps %}
```

Dòng đầu tiên, mấy con số nom có vẻ bí hiểm lần lượt là tọa độ (kinh, vĩ tuyến gì đó), mức zoom (4), chiều ngang (100%), chiều dọc (450px). Dòng thứ 2 là tọa độ điểm đặt icon mark và link tới icon.

[Mời xem](/about)