---
title: ladeoji 4.0
tags: [news, life, idea]
date: 2018-10-15
summary: truyền thông khoảng 1 năm trở lại đây ra rả cách mạng bốn chấm không. Nó là đéo gì?
preview: https://cdn-images-1.medium.com/max/1600/1*IDuLzsAoDpEBNwjwtH9PzQ.png
---

Tao nhớ lần đầu tiên nghe cụm từ này là trong cái xã luận tung hô bài diễn văn của anh thủ tướng, sặc mùi nịnh bợ nên cũng chả buồn ghi lại. Nghĩ nó cũng nhảm, kiểu “chính phủ kiến tạo”.

Cơ mà càng ngày càng thấy thiên hạ tung hô khoẻ. Nhẽ chú phỉnh quyết tâm nhồi cụm từ “cách mạng” này vào đầu nhân dân.

Tao vẫn cho rằng cái gọi là “cách mạng 4.0” chỉ trò tung hứng chữ nghĩa nhảm nhí của bọn văn phòng rỗi việc, không đáng quan tâm. Nay thấy [một ý kiến công khai trên báo đảng](https://vnexpress.net/tin-tuc/goc-nhin/ao-anh-bon-cham-khong-3823923.html), bàn góp tí.

Bọn ngoài vỉa hè đồn rằng nhân loại đã trải qua 9981 cuộc cách mạng gì đó. Đại khái có 9887 cuộc chưa có trong sử, rồi đến nông nghiệp, công nghiệp, thông tin và giờ là công nghệ. Mục đích là gì? Tại sao phải gán ghép khái niệm “cách mạng”, vào một thứ lẽ ra chỉ là “xu thế”, một cách khiên cưỡng, như thế?

Một xu thế được gọi là cách mạng, khi nó làm thay đổi triệt để hình thái xã hội, thâm chí làm đảo lộn hệ thống giá trị, thay đổi quan niệm, tư tưởng các cái. Nắn bóp kiểu đéo gì cũng không thấy “cách mạng” trong sự phát triển của công nghệ. Có chăng chỉ là làm cho kiếp lừa của nhân dân thêm chút phong phú. Những con lừa của thời đại kỹ thuật số luôn tự tin rằng những tiếng be be của chúng đủ để làm cách mạng.

Mà thôi kệ mẹ thiên hạ tung hô. Xét khía cạnh phổ cập bình dân thì cái này cũng chả hại gì. Cứ bằng lòng làm một con lừa kỹ thuật số vậy. Biết biến thành con đéo gì được bây giờ.