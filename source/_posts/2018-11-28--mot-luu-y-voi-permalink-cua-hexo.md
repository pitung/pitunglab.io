---
title: một lưu ý với permalink của hexo
tags: [hexo, permalink, tip, warning]
date: 2018-11-28 23:51:07
---

Thiết lập permalink mặc định trong `_config.yml` là `:year/:month/:day/:title`, url sinh ra kiểu này:

```url
http://localhost:4000/2018/11/26/tieu-de-bai-viet-moi/
```

Tao không ưa lắm, xoay sang `:year-:month/:title`, url thành

```url
http://localhost:4000/2018-11/tieu-de-bai-viet-moi/
```


Đoạn dưới này không biết do lỗi gì. Chỉ thấy trong một site thử nghiệm, những cái khác không dính.

Định nghĩa new post (`_config.yml`) tạng `:year-:month-:day-:title`, tên file tạo ra từ `hexo new "tên bài"` (commandline) sẽ là `2018-11-26-ten-bai.md`. Permalink lấy **đầy đủ** tên file *(bao gồm cả phần ngày tháng)* làm slug, nom rất tởm.

```html
http://localhost:4000/2018-11/2018-11-26-ten-bai/
```

Nhưng nếu định nghĩa `:year-:month-:day--:title` *(chỉ thêm 1 dấu gạch ngang thôi)*, thì tên file tạo ra là `2018-11-28--ten-bai.md`. Permalink tự động **lược bớt phần ngày tháng**, chỉ để lại slug là `ten-bai`.

```html
http://localhost:4000/2018-11/ten-bai/
```