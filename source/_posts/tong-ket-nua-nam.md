---
title: tổng kết nửa năm
date: 2018-06-05 12:27:22
tags:
  - think
  - write
---

Đầu óc loạn quá. Nghỉ ngơi sắp xếp lại chút.

Tao có nhiều mối quantâm. Những quan tâm đó chả liêncan deoji nhau. Điều này tao nhận thấy từ lâu. Vẫn cố sửa, đéo ăn thua ji mấy. 

Sở thích nhiều lắm. Vẽ cũng thích, làm craft cũng tích, webdev cũng thích, kiến trúc cũng thích, cơ khí cũng thích, thậm chí thỉnh thoảng cả âm nhạc và thể thao... Tóm lại đéo ji cũng ham.

Vấn đề này nhiều thằng gặp, nhưng ở tao nó nghiêm trọng bởi vì **đéo sở thích nào kiếm ra xèng**. Cái mang lại nhiều xèng nhất trước jờ là design, nhưng cũng chỉ trong fạmvi hẹp, fụcvụ in ấn - loại việc mang tính chất mùa vụ. Công việc bất ổn mang lại nhiều thời jan rảnh rỗi, những sở thích nhảm nhí càng được zịp phát sinh.

Đầu óc có hạn, trong khoảng thời jan nhất định, tao chỉ có thể quan tâm một thứ. Kinh nghiệm thường khoảng 3 tháng cho mỗi sở thích. Trong thời jan 3 tháng này, những thứ khác hoàn toàn bị loại ra khỏi đầu cho đến khi nó được kích hoạt trở lại.

Đó cũng là lý zo tao hay quên.

Đơn cử vizu. Trong 3 tháng tập trung webdev, tao hoàntoàn cóthể hiểu và sử zụng các công cụ webdev mới nhất. Sản fẩm làm ra có thể ứng zụng ở mức doanhnghiệp nhỏ trở xuống. Không tệ đối với nănglực cá nhân. Thế nhưng sau đó thì sao?

Một sở thích khác _(vì lyzo nàođó)_ được kích hoạt. Toàn bộ trí lực và thời jan của tao sẽ lại zành cho việc đó. Thứ tự cothe khác nhau, nhưng thường là hội họa, thủ công, kiến trúc, cơ khí và vài thứ nhảm nhí khác.

Vài câu tóm tắt, đọc có vẻ bình thường. Nhưng thực sự có thể bình thường sao? Thử nghĩ, đến lúc sở thích webdev kích hoạt trở lại, là mất bao lâu? Kiến thức trong đầu còn sót lại gì?

Cái {% post_link yearly-planer "kế hoạch năm" %} (gần như) đã fá sản là vizu điển hình. Tao vẫn nhớ thời điểm lập kếhoạch, tao đang ngồi trong quán cafe, không hề nghĩ tới khả năng đầu óc mình bị sở thích khác chi fối. Cho đến lúc này, tao nhận ra một điều: _nhìn xa trông rộng_, đối với những kẻ như tao, hoàn toàn là xa xỉ fẩm.

Không nhìn xa được, thì nhìn gần thôi vậy.

Có lẽ nhận biết về năng lực yếu kém của tao hiện jờ sẽ đem lại vài thay đổi (tích cực) trong thời jan tới. Đường fía trước còn zài, nhưng cũng bớt mù mờ hơn rồi.