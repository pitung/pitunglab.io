---
title: wednesday post
tags: [links, research]
date: 2018-08-21 23:07:12
summary: Tomtat va chiase lienket ve nhung vande tao qwantam.
preview: https://i.loli.net/2018/08/23/5b7eced130e4e.gif

---

Tiếp tục series "thứ tư ngọt ngào" hehe...  

{% post_link wednesday-post-01 "post đầu tiên đây" %}

---

## le apps

Cac app joithieu homnay deu chay duoc da-nentang, nghiala no co ca fienban cho Mac, Windows va Linux.

[marktext](https://github.com/marktext/marktext) — tao zung mon markdown nay jo thanh nghien roi. Khong ua noi batky phanmem bientap web nao khong zung duoc markdown.

[brave browser](https://brave.com/) — theo thongke, mot nguoizung internet trungbinh fai tieufi them 23$ moithang (tucla 276$ moinam) de download nhung thu balangnhang cha liencan deoji nhung thu ho can. Phan lon la quangcao va cac script theozoi hanhvi. Brave browser khoe la no nganchan duoc tatca nhung thu nhamnhi do.

[manta](https://manta.life/) — app chuyenzung lam invoice, nguoizung cothe tuybien jaozien trinhbay vanban cho fuhop voi style cua rieng ho. Xemqua cai joithieu, thay hinhnhu chi de trangtri cho dep, khong may huudung vi khongco tinhnang quanly hoadon.

## le me

Homnay tao gap fault voi dauoc tinhtoan cua minh. Nghi lai vua buoncuoi, vua xauho. Chala ngoi tanman tinhtoan cho muudo moi *(aka fotofun project)*, congtrunhanchia langnhang mot luc, ra ketqwa la mot conso thunhap khonglo. Nghibung fennay giauto me roi. Trong bung danhtrongthoiken tungbung mat ca mot buoichieu. Denluc fathien saisot, camthay dang tren thienduong roi huych fat xuong dat. Rat tham.

---

## update

Một số trải nghiệm của tao với các app được jới thiệu bên trên. Lưu ý chung là các apps (phần mềm) hiện đại có thể chạy được trên nhiều hệ điều hành thường được viết dựa trên [Electronjs](https://electronjs.org/). Hay ở chỗ thống nhất được giao diện và cách sử dụng trên nhiều nền tảng. Dở ở chỗ ngốn RAM rất khủng.

Đầu tiên là **brave browser**. Thằng này khoe nó chặn được hầu hết quảng cáo và các script theozõi hànhvi. Chạy thử thấy cũng khá nuột. Lượng ăn RAM so với Chrome cũng một chín một mười. Tính năng hay của nó là AdBlock được tích hợp nên không cần xài extensions. Một phát hiện khác là nó thể mở private tab kết nối với tor network. Thực tế ít dùng đến nhưng có cũng hay. Tóm lại **zùng tốt**.

Thứ hai là **mark text**. Thằng này ngoài cho người dùng một cái jaoziện tối jản để tập trung viết lách thì cũng khongcoji đặc sắc. Nếu công việc fải viết nhiều, kiểu *soạn tài liệu, viết blog* thì nên dùng. Tính năng hay của nó là có thể upload hình ảnh lên https://sm.ms/ - một cái host miễn phí (ở Hongkong thì fải). Tính năng biên soạn và highlight code dùng tốt. Tích hợp sẵn vài kiểu export thành file html hoặc pdf để lưu trữ. Tóm lại **dùng tạm được.**

Cuối cùng là **manta**. Đúng như tao hình dung, nó chỉ có chức năng tạo vài cái invoice đèm đẹp. Các tính năng quản lý quá đơn giản. Cá nhân dùng tốt, ápdụng ở cấp doanh nghiệp cần tùy biến sâu hơn, đặc biệt là template. Muốn điều chỉnh phải [kéo source code về](https://github.com/hql287/Manta) biên dịch lại. Dù sao app này cũng chỉ mang tính tham khảo là chính.

Chốt lại, chỉ giữ brave zùng thay Chome, còn lại remove tất.

Một vấn đề khác liên quan đến **Electronjs**, VSCode tao hay dùng cũng là dev trên nền tảng này. Tích hợp nhiều tính năng hay, dễ dùng, khởi động nhanh. Nhưng ngỗn RAM khủng quá. Chạy trên con PC cổ lỗ này hơi đuối. Quay lại với Sublime text cảm thấy dễ chịu hơn. Dù sao cũng không code thường xuyên.


