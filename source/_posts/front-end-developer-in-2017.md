---
title: a recap of front end developer in 2017
tags: [webdev, trend]
preview: 'https://cdn-images-1.medium.com/max/1600/1*aVzJTznRRfP1lM7AXe9yLw.jpeg'
refs:
  - title: recap of frontend dev 2017
    link: https://levelup.gitconnected.com/a-recap-of-front-end-development-in-2017-7072ce99e727
    from: gitconnected
date: 2018-08-02 16:05:23
summary: tổng kết 2017 của Trey Huffine
---

## 2017

- React 16. [what's new](https://edgecoders.com/react-16-features-and-fiber-explanation-e779544bb1b7)
- Progressive Web Apps [wtf it is?](https://en.wikipedia.org/wiki/Progressive_Web_Apps) (wikipedia), [wtf it is again](https://dev.to/amberleyjohanna/seriously-though-what-is-a-progressive-web-app-56fi) (dev.to)
- [Yarn](https://yarnpkg.com/en/), [pwa 101, what, why, and how](https://medium.freecodecamp.org/progressive-web-apps-101-the-what-why-and-how-4aa5e9065ac2)
- CSS Grid [css grid beginer guide](https://medium.freecodecamp.org/css-grid-the-beginners-guide-45998e6f6b8)
- WebAssembly
- Serverless architectures [(power of serverless for front end developer)](https://codepen.io/chriscoyier/project/live/ZepgLg/)
- Vue.js, [the vue handbook](https://medium.freecodecamp.org/the-vue-handbook-a-thorough-introduction-to-vue-js-1e86835d8446)
- CSS-in-JS (upcoming CSS holy war?)
- Static site
- GraphQL
- React Router 4
- Angular 4, and 5
- TypeScript and Flow

## 2018 (expect)

- Best way to handle styles in *component-based* applications (css).
- More mobile solutions (like React Native or Flutter).
- More native, offline capabilities, and seamless mobile (web).
- Better web experience (WebAssembly).
- GraphQL continues to challenge REST.
- React strengthens its position.
- JavaScript more structure (Flow and TypeScript).
- Containerization becomes more prevalent for front end architecture.
- VR makes strides forward (A-Frame, React VR, and Google VR).
- Some really cool blockchain and web3.js applications 
