---
title: in post image zooming effect
tags: [webdev, javascript]
refs:
  - title: npm package
    link: https://www.npmjs.com/package/medium-zoom
    from: npmjs
  - title: playground
    link: https://codepen.io/francoischalifour/pen/MEPrpX
    from: codepen
  - title: 107 sample of content card
    link: https://freefrontend.com/css-cards/
    from: freefrontend
date: 2018-07-30 13:12:25
---

Hiệu ứng trình bày hình ảnh trong bài viết.

Tìm mãi mới thấy một cái ok để dùng.

Trông nó như này: https://medium-zoom.francoischalifour.com/

Cách đơn giản nhất để áp dụng, là cứ nhét tạm vào file html thôi

```html
    
    ...

    <script src="/js/medium-zoom.min.js"></script> // đường dẫn
    <script>mediumZoom("p > img");</script> // đối tượng áp dụng
</body>
```

[medium zoom npm package](https://www.npmjs.com/package/medium-zoom)