---
title: những trò bẩn kinh doanh online
tags: [business, life, think]
refs:
  - title: thương trường khốc liệt
    link: https://baomoi.com/thuong-truong-khoc-liet-nhung-chieu-tro-do-ban/c/23403999.epi
    from: baomoi.com
  - title: giặc chuột - trò đốt tiền đối thủ cạnh tranh
    link: http://athena.edu.vn/click-tac-tro-ban-de-dot-tien-cua-doi-thu-kinh-doanh-o-viet-nam/
    from: athena.edu.vn
  - title: văn hóa khôn lỏi của người việt
    link: http://dantri.com.vn/dien-dan/su-te-hai-cua-van-hoa-khon-loi-20161001230305881.htm
    from: dantri.com.vn

date: 2018-08-08 13:04:44
summary:
---

Tao vẫn luôn tin rằng **ý thức** và **sự tử tế** trong việc cung cấp dịch vụ là điều tạo nên giá trị, chứ không phải giá rẻ và [những cuộc "đua top"](http://athena.edu.vn/click-tac-tro-ban-de-dot-tien-cua-doi-thu-kinh-doanh-o-viet-nam/) lấy được.

Kinh doanh ở VN, điều đáng sợ không ở phía người mua, mà là [những trò bẩn trong cạnh tranh](https://minhthoai.wordpress.com/2012/03/29/m%E1%BA%B9o-marketing-p14-ch%E1%BB%91ng-l%E1%BA%A1i-nh%E1%BB%AFng-tro-ch%C6%A1i-b%E1%BA%A9n-d%E1%BB%83-d%E1%BB%99c-chi%E1%BA%BFm-th%E1%BB%8B-tr%C6%B0%E1%BB%9Dng/) của người bán. 

Tao cũng từng từng xỉ vả thậm tệ một đàn em, khi thấy trên trang bán hàng của y cụm từ quen thuộc -- **giá: liên hệ**. Nghe xong, nó gãi đầu gãi tai, bảo:

"Đại ca đúng là ngu như lợn. Còn vụ **giá: inbox** nữa sao không nói cmn luôn đi. Đệ cũng muốn để giá công khai lắm. Nghĩ mình làm ăn đàng hoàng, thuế phí nộp đủ, cây ngay đéo sợ chết đứng. Nhưng cũng chỉ cố được vài tháng. Nhiều thằng phá đám lắm đại ca ơi. Thằng ở ngoài phá đã đành. Nhân viên mắt trước mắt sau cũng rủ nhau phá. Tổ này đội nọ đến hỏi thăm xoành xoạch xoành xoạch. Đệ không bỏ đăng giá công khai trên web, nhẽ giờ này đi ăn mày cùng đại ca cmnr..."

Ờ...! Nó có lý.

---

### nhặt nhạnh vài trò bẩn dân VN hay dùng

**treo đầu dê bán thịt chó**, bày hàng tốt bán hàng xấu, bày hàng thật bán hàng giả.

**ăn cắp thông tin khách hàng** của các shop uy tín rồi nhắn tin, gọi điện bán cùng loại hàng với giá rẻ hơn.

**tạo trang bán hàng mạo danh**, lừa đảo để hạ uy tín chính chủ.

**tạo chân gỗ** (liên kết với nhiều thằng khác), giả vờ tìm mua mặt hàng nào đó để bán chính mặt hàng ấy (cho các shop khác).

**copy hình ảnh, thông tin** của shop khác về đăng bán trên shop của mình (dĩ nhiên với giá rẻ hơn).

**đổi thông tin tra cứu của đối thủ** (điện thoại/email/web) trên các trang tra cứu thông tin (tạng wikipedia, google maps...) để gây nhiễu loạn.

**cố tình gây scandal**, tranh chấp, kiện tụng với các shop khác, đánh giá giả mạo, công kích cá nhân, dựng chuyện, vu khống.

**dùng chiêu bài "từ thiện"** để bán hàng kém chất lượng.

**dùng tài khoản giả mạo** hoặc thuê người nhận xét tốt về cửa hàng/sản phẩm của mình. Cố tình cung cấp thông tin giả, hoặc lờ đi những thông tin bất lợi về mặt hàng mình bán.

**giấu giếm phụ phí** (vận chuyển, đóng gói, phụ kiện...), khuyến mãi giả.

**làm phiền đối thủ cạnh tranh** bằng cách đưa số điện thoại của họ lên các trang khiêu dâm.

---

Còn mơ VN nên rồng nên hổ nữa, hay không?