---
title: suzung slack
date: 2017-12-25 16:30:47
tags: [regex, slack, automation, blackberry]
---

### update 2

Làm xong con regex trước thấy déo ổn. Thongtin dòuvào zàizòng vôtichsự. Ngắn lại chút nữa dỡ mât công typing.

Như nài

```text
chi 50 ăn sáng & cafe
thu 350 chuyển hàng cho khach
```

Con regex mới như nài

```regex
(?<action>chi|thu)\s*(?<amount>\d*)\s*(?<desc>.*\r*)\r*
```

### update 1

Text dòuvào viet theo chuẩn này

```text
put 200 on eating with lovely slut

// or

get 2500 from selling condoms
```

Con regex này tôt hơn

```regex
(?<group1>put|get)\s*(?<group2>\d*)\s*(?<group3>\w*)\s*(?<group4>.*\r*)\r*
```

Tại sao tôt hơn? Có zời biết. Ta màymò làm ra rồi tự chorằng thế. Khi zùng nên dặt tên group cho fùhợp.

---


[Slack](https://slack.com/) trên con BB lởm đã qwá cũ. Logout rồi không login lại được nữa. Đành fải tìm cách reinstall. Loayhoay mât cả buổi sáng, tưởng fải reinstall cả OS. Mãi sau mới biêt cách tải file `.apk` về cài.

Slack chạy ngon trên smartphone, nghĩa là jờ có thể zùng nó trên hòuhêt các fươngtiện hiện có. Tính sẵnsàng không cao như email, nhưng coinhư thêm fươngtiện nghịchngợm.

Ta thiêtlập thêm một scenario trong integromat cho theozõi một kênh private. Nếu message fùhợp kufap, nó sẽ lọc thongtin rồi ghi vào googlesheet.

Sodo như này:

[![](http://res.cloudinary.com/bidong/image/upload/c_scale,w_650/v1514195300/Listen_to_slack_Integromat_kiogeg.png)](http://res.cloudinary.com/bidong/image/upload/v1514195300/Listen_to_slack_Integromat_kiogeg.png)

Dayla doạn regex lọc content *(It works, but stupid)*

```regex
(put|get)\s*(?<amt>.*?)\s*(on|from)\s*(?<desc>.*)\r*
```

Integromat **mienfi** cho chạy 1000 operation/month, trung bình mỗi ngày 33 fat. Nhu cầu ghichep chitieu mỗi ngày 5 fat, nhiều lắm thì cũng chỉ gâp 3 lần. Thela thoải mái zùng rồi.