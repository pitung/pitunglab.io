---
title: hexo post file name
tags:
  - hexo
  - problem
refs:
  - title: hexo writing
    link: https://hexo.io/docs/writing
    from: hexo.io
date: 2018-08-03 11:38:00
summary:
---

Tạo bài mới bằng command line: 

```text
hexo new "file-name"
```

Theo mặc định, hexo sẽ tạo file `[file-name].md` trong `_posts` folder.

Mở trong code editor (sublime / vscode), các bài viết thường sắp xếp theo tên file. Tao thấy cách đặt tên file jống như Jekyll, nghĩa là tên file dạng `year-month-date-title.md`, hợp lý và dễ tìm hơn.

Sửa `_config.yml`

```yaml
# default is :title.md
new_post_name: :year-:month-:day-:title.md
```

Làm xong chạy thử thấy lỗi, đoán do conflict với **permalink**

Sửa tiếp `_config.yml`

```yaml
# default is :year/:month/:day/:title/
permalink: :year:month/:title/
```

Thế thôi.