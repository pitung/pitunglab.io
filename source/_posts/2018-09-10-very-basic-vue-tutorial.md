---
title: cơ bản về vue
tags: [webdev, vue, tutorial]
date: 2018-09-10 08:19:42
summary: kiến thức cơ bản tác dụng không lớn khi làm project tạng copy paste, nhưng nếu va vấp trong quá trình làm mà không có cơ bản thì không biết hướng giải quyết.
preview: https://techtalk.vn/wp-content/uploads/2017/02/68747470733a2f2f7261776769742e636f6d2f7675656a732f617765736f6d652d7675652f6d61737465722f6c6f676f2e706e67-696x487.png
refs:
  - title: lý do vue ăn đứt react
    link: https://techtalk.vn/ly-do-nao-khien-vue-js-an-dut-react-js.html
    from: techtalk.vn
---

Dựng site tạng copy paste kiểu trước đây hay dùng có vẻ không còn phù hợp. Cấu trúc của nó không chỉ cần một vài file để chạy. Một con webapp giờ có cấu trúc rất phức tạp. Không nắm được cấu trúc (nguyên lý) cơ bản của nó, đừng mong làm được cái gì hay ho.

[from zero to hero with vue](https://medium.com/@wesharehoodies/from-zero-to-hero-with-vue-up-and-running-f1acd4696b71) — loạt bài giúp người dùng hiểu cấu trúc cơ bản của vue app và các thuật ngữ hay gặp. Ví dụ tưng bước khá dễ hiêu, dễ theo dõi.

[bài đầu của loạt 5 bài trên css-trick](https://css-tricks.com/intro-to-vue-1-rendering-directives-events/) — tay viết tuts trên css trick cũng có cách trình bày dễ hiểu, có ví dự bằng codepen để tham khảo.
