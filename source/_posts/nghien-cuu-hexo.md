---
title: nghiên cứu hexo???
date: 2017-11-16 10:49:27
tags: [hexo, learn, photo]
category: webdev
---

Chưabaojờ dểý nghiêncứu Hexo một cách tử tế. Mỗilần zùng lại nhặt tạmbợ một theme vớvẩn nàodó, modify tíchut cho hợp măt, rồi... vưt cbn mât.

<!-- more -->

Thựctế, so các app zựng static site, Hexo dầydủ hơn cả, từ chia category, archive, pagination. Theme zựng sẵn lại nhiều, [bộ content tag fongfú và tiệnzụng](https://hexo.io/docs/tag-plugins.html),...

Vizu chèn hình ảnh vào bài cothe suzung cach thongthường:

```markdown
![](/path/to/image)
```

html render thành 

```html
<p>...</p>
<p><img ... /></p>
<p>...</p>
```

![](https://res.cloudinary.com/bidong/image/upload/v1513668379/prv/fashion_1_usie2v.jpg)

hoặc zùng tag

```text
{% 
    img [class names] 
    /path/to/image 
    [width] [height] 
    [title text [alt text]] 
%}
```

Khi zùng tag, hìnhảnh không là một fần nộizung. Html render thành:

```html
<p>...</p>
<img ... />
<p>...</p>
```

như này (click chuột phải, view page source):

{% img name https://res.cloudinary.com/bidong/image/upload/v1513668379/prv/fashion_1_usie2v.jpg "beauty" %}

Nom ổn fết nhẻ!!!