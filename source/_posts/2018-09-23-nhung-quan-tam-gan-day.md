---
title: tóm tắt vài thứ đang quan tâm
tags: [study, hugo, emacs]
date: 2018-09-23
summary: quan tâm nhiều thứ quá đâm lười viết. Cả chục năm rồi vẫn chưa tìm được giải pháp ổn thoả.
preview: https://www.invaluable.com/blog/wp-content/uploads/2018/08/Edward-Weston-Cabbage-Leaf.jpg
---

Mấy tuần vừa rồi phần vì công việc bận rộn, phần vì lười nên không sờ đến lôc. Lý zo vẫn như trước: không biết sắp xếp thông tin cần viết sao cho mạch lạc.

Nhặt được câu phát ngôn buồn cười vãi

![](https://res.cloudinary.com/practicaldev/image/fetch/s--xtubDFiX--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_66%2Cw_880/https://raw.githubusercontent.com/adnanrahic/cdn/master/image-resize-on-the-fly/googlepage2.gif)

Những sự tình diễn ra gần đây:

- bận rộn với tờ gấp của khách, đổi đi đổi lại mấy lượt vẫn không xong.
- đăng ký dùng thử vài social network tạng Mastodon, thấy nhảm.
- tạm ngừng tìm hiểu vue (nhiều thứ bất tiện, ít món ăn sẵn)
- Dùng lại [Hugo](https://gohugo.io/) thấy hay. Thiết lập quy trình đánh site mới.
- Nghiên cứu hugo theme và cách sắp xếp nội dung trên hugo site (templating)
- Mò thấy [caddy server](https://caddyserver.com/), thấy hay hay lại lấy về chạy thử.
- Đọc thấy có thằng làm content cho hugo site bằng emacs (+ org-mode) rất tiện, muốn bắt chước.

### đọc

[hành trình học code](https://blog.siliconstraits.vn/hanh-trinh-hoc-code/) được biên dịch từ [bài này](https://www.thinkful.com/blog/why-learning-to-code-is-so-damn-hard/) — mô tả chính xác những giai đoạn mà một coder sẽ trải qua. Một hành trình vô cùng gian nan và rất dễ lạc lối. Tiện thể, tao đang lang thang trong giai đoạn gọi là “sa mạc tuyệt vọng”. Chưa thấy đường ra đâu sất.

[Emacs tutorial](http://www.newthinktank.com/2017/11/emacs-tutorial/) -- Không fải official tuts, tổng hợp các phím tắt để làm quen. Có clip hướng dẫn kèm theo. Tải emacs về máy (pc, win7) extract ra thấy dung lượng nó 677MB. Phần mềm với dung lượng khủng thế này chắc chắn không chỉ dùng để gõ vài cái text như tao đã hình dung.

[hugo 0.49](https://gohugo.io/news/0.49-relnotes/) có thể dùng cả thư mục để làm archetype (tạng content tempalate, giống scarffold của hexo).

[graphQL visual editor](https://github.com/slothking-online/graphql-editor) — với những đứa có hệ thống thần kinh cấu tạo đơn giản như designer thì vẽ mọi thứ ra là cách hữu hiệu nhất để hiểu vấn đề.