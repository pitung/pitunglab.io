---
title: setting up vuejs with airtable
tags: [vue, api, webdev, airtable]
refs:
  - title: basic intro airtable api
    link: https://medium.com/row-and-table/an-basic-intro-to-the-airtable-api-9ef978bb0729
    from: medium.com
date: 2018-08-03 16:37:55
summary: dùng vuejs đọc dữ liệu airtable qua api
---

Chạy [Gatsby](https://www.gatsbyjs.org/) không được. Quay sang thử với [Vuejs](https://vuejs.org/) theo hướng dẫn [ở bài này](https://medium.com/row-and-table/an-basic-intro-to-the-airtable-api-9ef978bb0729).

Chỉ là một cái giới thiệu sơ lược, để có khái niệm về cách kết nối và lấy dữ liệu qua API. Thực tế chỉ cần một file html là đủ. Sẵn đang chạy hexo nên tạo luôn một trang để thử

### html

```html
<div id="app">
<h1 class="site_title">The first time with vuejs</h1>
  <div class="card_list">
    <div class="card_item" v-for="item in items">
      <h3>{{ item['fields']['Item'] }}</h3>
      <p>{{ item['fields']['Description'] }}</p>
      <p>Price:${{ item['fields']['Price'] }}</p>
      <p>Category:{{ item['fields']['Category'] }}</p>
      <img :src="item['fields']['Photo'][0]['thumbnails']['large']['url']" alt="" v-if="item['fields']['Photo']">
    </div>
  </div>
</div>
```

### javascript

```javascript
<script type="text/javascript" src="https://unpkg.com/vue"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<script type="text/javascript">
    var app = new Vue({
        el: '#app',
        data: {
            items: []
        },
        mounted: function () {
            this.loadItems();
        },
        methods: {
            loadItems: function () {
                // Init variables
                var self = this
                var app_id = "YOUR-TABLE-ID";
                var app_key = "YOUR-API-KEY";
                this.items = []
                axios.get(
                    "https://api.airtable.com/v0/" + app_id + "/YOUR-TABLE-NAME?view=Grid%20view",
                    {
                        headers: { Authorization: "Bearer " + app_key }
                    }
                ).then(function (response) {
                    self.items = response.data.records
                }).catch(function (error) {
                    console.log(error)
                })
            }
        }
    })
</script>
```

Có 3 chỗ (viết hoa trong code) cần thông tin lấy tử airtable

- API key `YOUR-API-KEY`: lấy theo hướng dẫn
- table ID `YOUR-TABLE-ID`: lấy theo hướng dẫn
- table name `YOUR-TABLE-NAME`: Lưu ý **tên table**, hướng dẫn theo link đưa đầu bài không đề cập đến chỗ này, loayhoay mất một lúc mới nhìn ra.

Chạy được và lấy được data mới là bước đầu tiên, Thực tế chỉ để thoả mãn sự tò mò, theo kiểu *“À, hoá ra thế”*, mà thôi. Để nâng level lên còn cần nhiều thời gian.

---

### update

Giải pháp upload hình ảnh lên cùng với airtable không hay lắm hợp với việc quản trị, nhưng không hợp lý nếu làm trang web. Tao thêm một cột `previewlink` (type: url) để copy đường dẫn tới link ảnh từ nguồn khác.

```html
<div id="app">
<h1 class="site_title">The first time with vuejs</h1>
  <div class="card_list">
    <div class="card_item" v-for="item in items">
      <h3>{{ item['fields']['Item'] }}</h3>
      <p>{{ item['fields']['Description'] }}</p>
      <p>Price:${{ item['fields']['Price'] }}</p>
      <p>Category:{{ item['fields']['Category'] }}</p>
      <img :src="item['fields']['previewlink']" alt="" v-if="item['fields']['previewlink']">
    </div>
  </div>
</div>
```