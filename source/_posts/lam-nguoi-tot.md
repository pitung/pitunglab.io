---
title: làm người tốt???
date: 2018-06-08 11:56:01
tags:
  - think
  - idea
---

Chổng mông vào lịchsử fát

![](http://res.cloudinary.com/bidong/image/upload/v1528443914/chongmong2_ifqjpb.jpg)

Lịch sử, là zo bọn thắng trận biên ra. Cái này đéo ai cũng biết. Trong sử, vịtrí bọn thắng cuộc luôn ở bên sáng ngời chínhnghĩa. Lãnhtụ xưa, tuyênbố quyềnlực chúng zo ý trời. Lãnhtụ nay, tuyênbố quyềnlực chúng zo lòng zân. 

Bấtkể lýzo nào, lãnhtụ fải là “người tốt”. Jáokhoa biên thế. Chúng mài tin thế, phỏng???

Tao đéo cãi. Trước quyềnlực tuyệtđối (*), mọi sự fảnkháng là vô nghĩa.

---

Tao biên đây vài ý tưởng, thẩm tới đâu tuỳ tâm chúng mài:

## #1

Thống khổ zo “người xấu” đem lại, nhưng bikịch nhânjan luôn zo “người tốt” gây nên.

## #2

Khi chúng mài tựhào mình đứng bên chínhnghĩa và cămthù sâusắc kẻ thù của lãnhtụ, đó là lúc chúng mài sắp gây nên taihọa.

## #3

Người tốt nắm quyền lãnhtụ là thảmhọa cho cả zântộc.

## #4

Mọi hànhvi chănzắt đều hướng đến mụcđích **làm thịt**.

---

Tạm thế đã. Tóm lại làm người tốt chả ích lợi đéo ji.

Hãy ghi nhớ: **Tính toán dựa trên cơ sở lợi ích, trong mọi trường hợp, đều dẫn đến kết quả đúng.**

---

(*) Đừng cãi. Thứ zuynhât cothe gọi là _"quyền lực tuyệt đối"_, chính là tưzuy _bên trong củ sọ_ của lũ đầu bò chúng mài.