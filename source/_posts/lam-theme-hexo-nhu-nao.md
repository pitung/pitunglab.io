---
title: làm theme hexo như nào?
date: 2018-06-05 13:49:49
tags:
  - hexo
  - theme
  - style
  - problem
  - css
---

Định sửa vài chi tiết jaoziện, jở ra jật hết cả nẩy. Cảm jác húc mẹ đầu vào tường. Điếu thấy file `.css` nào sất, thấy mỗi file `sytle.styl`. Trong đầu tự zưng nảy ra ý nghĩ "Thôi bỏ mẹ rồi! Zùng stylus là fải chạy commandline + đường zẫn để nó generate ra cái file `.css` tương ứng cơ mà? Thế cái file đấy sẽ cho nó nằm đâu?".

Ngớ ngẩn nhẽ fải nửa canh jờ, chợt nhớ ra năm ngoái hình như cứ thế làm chứ có cần chạy stylus đâu. Thử fát đúng chóc.

Hóa ra, chỉ cần chạy hexo là được rồi. Dở hơi thật.