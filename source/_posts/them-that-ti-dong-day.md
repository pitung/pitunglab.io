---
title: thêm thắt tí động đậy
date: 2018-06-09 16:28:47
tags:
  - hexo
  - theme
  - javascript
---

Mạng chán, điếu thấy trò ji hay. Tao lại jở lốc ra nghịch.

Nom cái jaoziện tinh chữ nhẽ buồn, Cơ mà điếu muốn bàybiện nhìnhằng. Thuổng code của [thằng này](https://codepen.io/billimarie/pen/mJLeBY) làm cái background động đậy tí cho vui mắt

Jở `layout.ejs` ra tống code của nó vào đấy, rồi fếtfẩy lại tí trong `style.styl`. Kết quả nom cũng tàm tạm.

![](http://res.cloudinary.com/bidong/image/upload/v1528537181/2018-06-09_163801_iye6wl.png)

Chuột đi đến đâu, background động theo đến đấy. Vui fết!

---

Zĩnhiên ápzụng luôn cho [một trang khác](http://vietkhanhmedia.vn)

[![](http://res.cloudinary.com/bidong/image/upload/c_scale,w_650/v1528703775/2018-06-11_145530_plozmd.png)](http://res.cloudinary.com/bidong/image/upload/v1528703775/2018-06-11_145530_plozmd.png)