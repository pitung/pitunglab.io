---
title: thay giao diện mới
tags:
  - hexo
  - theme
  - guideline
refs:
  - title: buonban theme
    link: https://buonban.netlify.com/
    from: buonban
date: 2018-08-03 09:10:54
summary: không khó nhưng có mấy chỗ cần nhớ, ghi lại cho khỏi quên
---

Tao vừa build thử nghiệm một hexo site khác. Làm theme cho nó xong muốn áp dụng sang site này.

### Check list:

- copy (và/hoặc sửa nội dung) các file scaffolds
- copy nội dung file `package.json`
- copy nội dung file `_config.yml`
- copy giao diện mới vào thư mục `themes`
- `cd /path/to/site`
- `npm install`
- `hexo server`
- kiểm tra, sửa post cũ theo scaffold mới (nếu cần)

### vài lưu ý khác

- categories (post type) `selling` & `collection`
- `/about` page (có sử dụng google maps)
- api của google (`_config.yml`)
- prism plugin options (`_config.yml`)

## some note about this theme

- card-base design
- flexbox
- font-awesome icon
- optional post preview, summary, reference link
- medium like image zoom
- css grid for displaying image (*collection* and *selling* post type)
- google maps with *customize* place icon
- differentiate post style on home page and post layout
- archive link and site tags on bottom
