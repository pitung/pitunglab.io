---
title: from hexo to hugo
tags: [webdev, hugo, hexo]
date: 2018-09-25
summary: hexo dùng hay nhưng có vẻ không đủ động để làm webshop. Tao đang ý định chuyển sang hugo
preview: https://cdn.photographylife.com/wp-content/uploads/2018/09/High-Contrast-Black-and-White-Abstract-Photography.jpg
---

Cấu tạo hai thằng không khác nhau mấy tuy dùng hai thứ ngôn ngữ khác nhau. Hugo lợi thế ở tốc độ, hexo lợi thế ở plugins.

Cài đặt Hugo dễ hơn. Chỉ cần tải cả cục về ném vào xó nào đó trong HDD rồi dẫn `PATH` vào là xong. Hexo chỉ chạy khi có nodejs. Cài đặt cũng nhanh nhưng đám dependencies trong node_modules nom ngứa mắt.

Hugo có vẻ “đa năng” và “động” hơn trong xử lý dữ liệu. Tự tạo shortcode trong hugo dễ hiểu dễ làm hơn nhiều so với hexo. (cái này hexo phải làm bằng plugin hoặc dùng file javascript jido). Template syntax của hugo nom cũng sáng sủa hơn.

Hexo có một plugin tao đặc biệt thích (tuy ít dùng), đó là Hexo Hey (hoặc hexo admin). Nó làm cho việc chèn hình vào bài viết dễ chịu hơn rất...rất...rất nhiều.

Hexo nằm trong hệ sinh thái của nodejs. Quen thuộc với javascript thì nên chọn hexo. Thiếu tính năng gì có thể tự bổ sung lấy. Nhưng nếu trình độ javascript chỉ ở mức cơ bản, nghĩa là chỉ biết dùng `js` files bằng copy-paste thì chuyển qua dùng hugo cho nhanh.

Chuyển qua, nghĩa là dev một con lôc khác với nội dung khác. Con lôc này vẫn dùng hexo như cũ. Sao fải dùng một khi có thể dùng hai? Có thằng nào đánh thuế hay thu tiền bảo kê điếu đâu.