---
title: "những toan tính mới"
date: 2018-05-31 13:13:00
tags: 
  - write
  - think
---

Hơn năm qua chả được việc đéo jì ra hồn. Lười quá. "Hang ổ" vẫn bừa bãi và bụi bặm. Hô quyết tâm cả nửa năm, cuối cùng mọi thứ... nguyễn y vân.

Một trong những lyzo chậm trễ, là công đoạn xử lý thô (jaiđoạn chuẩn bị vậtliệu) quá yếu. Tao đã sailầm khi không đầutư tốt cho công đoạn này. Hậuquả nhiều fần việc không thể làm được zo không có công cụ thích hợp. Vizu như việc cắt fôi không thể thực hiện trên máy CNC zo kích thước quá lớn hoặc thời jan chuẩn bị quá mất công.

Ngở ngẩn mất gần tháng, trong đầu tòi ra ý tưởng làm cái máy cắt tạng DIY, để jacong fôi. Sau một thời jan chuẩnbị, đồ đạc cũng coi như đủ. Khá tốn kém.

Thế nhưng tao đã **quá cảm tính** khi cho rằng chỉ cần đủ linhkiện là cothe lắp ráp. Nghĩ sâu một chút, tự tay lắp ráp (từ linh kiện) chỉ là _hứng thú nhất thời_. Có rất nhiều mặt fải tính đến (sự an toàn, công suất, độ chính xác...) Để thực hiện được tất cả những điều đó, tao fải mua thêm nhiều công cụ hỗ trợ. Hầu hết những công cụ này, sau khi lắp ráp, trở thành thừa thãi.

Hoàn toàn cothe nói rằng những thứ tao đã mua về đang trở thành một đống rác khó chịu.

---

Lưu lại các bản vẽ ở đây, fòng trườnghợp zùng đến

![img](http://res.cloudinary.com/bidong/image/upload/v1527747968/01_zlwn09.jpg)

![img](http://res.cloudinary.com/bidong/image/upload/v1527747968/02_j1gzy7.jpg)