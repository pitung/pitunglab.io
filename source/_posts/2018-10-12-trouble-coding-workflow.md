---
title: messy code — sửa hay xây mới?
tags: [code, style, trouble]
date: 2018-10-12
summary: dựng site xong muốn sửa lại style & template cho gọn gàng sáng sủa, thấy làm lại cái mới từ đầu còn tốn ít công sức hơn
preview: https://www.handshakeproject.com/wp-content/uploads/2017/09/Process-1080x675.jpeg
---

Thường xuyên gặp phải tình trạng dựng xong site, chạy một thời gian thấy có vấn đề phát sinh hoặc thấy có cách giải quyết vấn đề hay hơn, muốn sửa.

Đây là thời điểm gây khó chịu nhất.

Lúc dựng lên chỉ mong nhanh chóng làm cho mọi thứ hoạt động, chạy ra được là tốt rồi nên việc đặt tên class làm qua loa cho xong. Lúc tìm lại húc vào một đám bùng nhùng, đéo biết đường nào mà lần. Cảm thấy thời gian, công sức bỏ ra sửa thà dev cái mới còn nhanh hơn.

Vấn đề lớn, là dev cái mới cũng vẫn gặp tình trạng như thế. Rồi cũng sẽ đến lúc vấn đề mới nảy sinh. Không lẽ quay lại từ đầu?

Làm chuẩn ngay từ đầu được thì đã thành coder chuyên nghiệp. Mà tao cũng không chắc bọn coder chuyên nghiệp không gặp phải tình trạng này.

Giờ tính sao?