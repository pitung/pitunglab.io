---
title: fotofun service update
tags: [service, idea]
date: 2018-08-18 04:26:31
summary: Làm content cho site thực sự khó. Đừng tưởng lên mạng xem thằng khác làm rồi áp dụng cho mình là dễ. Cần rất nhiều thứ support cho ý tưởng, không chỉ là chuyện tiền nong.
---

Tiếp theo {% post_link dich-vu-chinh-sua-cat-cup-hinh-anh-cho-cac-shop-online "bài này" %}

Ngồi cả chiều mới ỉn ra vài cái hình, vẫn chưa có gì rõ ràng lắm.

![](https://res.cloudinary.com/bidong/image/upload/v1534540955/pitung.gitlab.io/0816/shop5.jpg)

![](https://res.cloudinary.com/bidong/image/upload/v1534540938/pitung.gitlab.io/0816/shop6.jpg)

Mấy tuần liền lăn lóc với mớ hỗn độn mang tên Gatsbyjs (thỉnh thoảng thêm cả Vue). Giờ đang cảm thấy ăn không tiêu. Thực tế là nếu quen rồi thì quy trình làm của nó rất hay. Mọi thứ đều là component. Hay ở đấy, mà dở cũng ở đấy. Với người mới làm quen, việc chia nhỏ code thành component cực kỳ khó theo dõi. Tao thường xuyên lẫn lộn, không biết cái nào làm gì và nằm ở đâu. Định vị được một cái lại quên tuột cái trước đó. Rất khó chịu. Phần lớn thời gian, vẫn chỉ dùng để dò dẫm.

---

Trong lúc làm vẫn miên man suy nghĩ về vấn đề đưa sản phẩm lên web.

Làm một trang tổng hợp, bày bán đủ thứ sản phẩm là việc hoàn toàn có khả năng. Vấn đề là tao đang cảm thấy mình hơi tham lam và phiến diện khi suy nghĩ như vậy.

Hãy nhìn thực tế. Với người muốn mua một món đồ gì đó mà không biết ở đâu bán, ở đâu tốt. Thì đương nhiên họ sẽ lên web để kiếm. Một gian hàng kiểu nhỏ lẻ, tự mở ra ở một xó xỉnh nào đó trên cõi mạng bao la, làm SEO đến bao giờ mới được người ta để mắt đến? Cứ cho là có công cụ và kiến thức về SEO đi, lấy gì để đua với mấy trang thương mại điện tử chuyên nghiệp với nguồn lực hùng hậu?

![](https://res.cloudinary.com/bidong/image/upload/v1534607254/pitung.gitlab.io/0816/job-search-1030x496.jpg)

Mở hàng ở chợ, may ra còn có người hỏi. Mở hàng trên internet. Khả năng ngồi ngáp ruồi là rất rất rất lớn. Thực tế đấy. Chả dễ chịu gì đâu.

---

## update quả process diagram

![](https://res.cloudinary.com/bidong/image/upload/v1534690087/pitung.gitlab.io/0816/Untitled_Diagram_2.png)