---
title: headless ecommerce
tags: [business, webdev, concept]
refs:
  - title: 2018 ecommerce predictions
    link: https://public.four51.com/2018-ecommerce-predictions/
    from: four51
date: 2018-08-17 00:40:59
summary: nền tảng headless đang được coi như thế hệ kế tiếp của B2B, nó là gì?
---

Khái niệm này mới (là mới với tao, chưa chắc mới với người khác), đọc rất thú vị.

Tao đang hình dung nó jống như một tạng doanh nghiệp có thể **biến hoá** cực kỳ linh động để phù hợp với hoàn cảnh, chứ không tuân thủ cứng nhắc theo một cái logic hoặc nền tảng “đần độn” nào đấy (dĩ nhiên vẫn dựa trên một cơ sở dữ liệu chung).

![](https://pbs.twimg.com/media/DeikuHGUQAAqx6L.jpg)

Nhặt tạm vào đây vài link có liên quan. Đọc sau:

- [2018 ecommerce predictions](https://public.four51.com/2018-ecommerce-predictions/)
- [headless ecommerce guide](https://ordercloud.io/headless-ecommerce-guide/)
- [headless ecommerce benefits](https://blogs.mulesoft.com/dev/industries-dev/headless-ecommerce-benefits/)
- [The Future Is Headless](https://magento.com/blog/best-practices/future-headless)
- [Headless eCommerce Toolkit](https://medium.com/@piotrkarwatka/headless-ecommerce-toolkit-f243e39e78b4)
- [Headless Commerce Explained in 5 Minutes](https://www.coredna.com/blogs/headless-commerce)
- [The Headless Commerce Showdown](https://www.bigcommerce.com/blog/headless-commerce/)
- [Headless CMS vs Traditional CMS](https://hackernoon.com/headless-cms-vs-traditional-cms-521ad6fda420)

---

Đọc hết đống link trên, kể cả đọc lướt, cũng tạm coi như có khái niệm về headless platform và ảnh hưởng của nó tới môi trường kinh doanh.

Rồi sao nữa? Ứng dụng?

---

Đang mò mẫm, [Xem bài này](https://snipcart.com/blog/react-graphql-grav-cms-headless-tutorial) chợt nhớ đã từng nghịch [Grav](https://getgrav.org/) thời gian khá dài. Quên tiệt rồi, nhưng mấy cái từng làm vẫn đâu đó trên Bitbucket. Ghi tạm lại đây để xem đợt tới có ứng dụng gì được không.

