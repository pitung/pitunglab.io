---
title: 'reserching print shop design'
tags: [research, design, copywriting]
refs:
  - title: printful homepage
    link: https://www.printful.com/
    from: printful
date: 2018-08-07 10:05:29
summary: design & copywriting
---

Mô hình kinh doanh này không mới, nhiều người đã làm. Ngay tại VN cũng có những đơn vị cung cấp dịch vụ kiểu này với giá thành rất rẻ. Ở đây không nói tới mô hình kinh doanh. Chỉ là vài ghi chép về cách trình bày nội dung site.

Post cái ví dụ với từ khóa *"in ấn theo yêu cầu"*.

![](https://res.cloudinary.com/bidong/image/upload/v1533613233/pitung.gitlab.io/0806/sample_page_4.png)

Tao nhặt trang này vào đây, chỉ đơn giản vì nó *"ngồi mâm trên"* trong cuộc đua SEO. Cả tính năng lẫn thiết kế đều rẻ tiền và ~~vô tích sự~~ (có lẽ) hợp với đám đông.

Các trang tạng "mâm trên" khác cũng tương tự. Dân VN sẵn sàng bỏ tiền mua vị trí, nhưng không bao giờ muốn bỏ tiền làm cho mình **xứng đáng** với vị trí đó. Họ chỉ cần 3 thứ: **rẻ, rẻ nữa, rẻ mãi**. Những thứ tử tế chắc không thiếu, nhưng chúng bị đạp xuống tận trang thứ n+1 nào đó. 

Ahem!... nếu muốn biết cảm giác thế nào là *nhân phẩm bị chà đạp*. Hãy đến VN và... sống tử tế.

Hãy so sánh với [printful](https://www.printful.com/). Xiênlổi printful, tao thật sự rất áy náy, khi đem mày so với đống cứt trên trển.

![](https://res.cloudinary.com/bidong/image/upload/v1533615480/pitung.gitlab.io/0806/sample_page_5l.png)

Vấn đề chỗ này *không ở tiền bạc*, mà là ý thức.

---

**Thiết kế**

- sáng sủa, rõ rành mạch
- thống nhất visual style
- dễ nhận biết các mục thông tin quan trọng

**Nội dung**

- đơn giản, ngắn gọn, trực tiếp
- trình bày dễ hiểu
- sơ đồ giản lược và video hướng dẫn
- giá cả minh bạch, chi tiết

**Tính năng**

- sample template tuyệt vời
- liên kết với các trang bán hàng thông dụng

---

![](https://res.cloudinary.com/bidong/image/upload/v1533615743/pitung.gitlab.io/0806/printful_flow.png)

![](https://res.cloudinary.com/bidong/image/upload/v1533615968/pitung.gitlab.io/0806/sample_page_6.png)