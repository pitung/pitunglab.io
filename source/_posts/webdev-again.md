---
title: xét lại quytrình thựcthi webdev
tags:
  - write
  - think
date: 2018-05-29 11:09:09
---

Bỏ bê mấy tháng trời, lăn lóc với các project khác, những thứ đã biết rơi rụng cbn hết. Jờ xét lại, thấy không nên lan man customize vớ vẩn. Vừa mất thì jờ, vừa làm công việc trở thành mớ bòngbong.

Gần nửa năm trôi qua, {% post_link yearly-planer "kế hoạch năm" %} thực thi chưa đến 1/3. Quan trọng nhất là các fương án make money chả có đéo ji tiến triển. Cảmjác rất tệ.

Tao đang gặp rắc rối. Những thứ đã làm trước đây hầu như đã trôi khỏi memory. Xem lại trang web đã làm cho khách, thấy nhiều thứ ngớ ngẩn. Tính toán qua, thấy **sửa lại** và **làm mới** đều _mất thời jan_ như nhau. Hìnhzung quãng thời jan cắm mặt vào code quả thật hãi hùng. Tạm gác việc này lại đã. Tuy nhiên nếu fải làm, tao sẽ lựa chọn fương án làm mới. **Hexo, gitlab/github, netlify** có vẻ fù hợp. Zù sao yêu cầu của khách cũng không quá cầu kỳ.

Trước tao vẫn zùng [sass](https://sass-lang.com/guide) để làm style cho website. Nay zự định chuyển qua [stylus](http://stylus-lang.com/), dù sao nó cũng dựa trên nodejs, đỡ mất công mò mẫm lằng nhằng.

Ý định tìm hiểu React thôi bỏ. Quá mất thì jờ. Không fải tay chuyên, cũng không zùng đến thường xuyên. Những thứ quá rắcrối không fải lựachọn tốt. Đọc qua chút giết thì jờ thôi.

Gần nửa năm qua có nhiều thứ tao ghichép bằng ghost trên localhost. Mọi thứ suônsẻ cho đến khi HDD đùng fát chết. Kinhnghiệm xương máo, là ghichép online hơi mất thì jờ chút nhưng antoàn hơn nhiều. Cái já fải trả cho những hamhố màu mè đôi khi hơi quá đắt đỏ :((