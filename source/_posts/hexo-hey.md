---
title: hexo hey
date: 2018-06-05 11:34:18
tags: [hexo, admin, github, problem]
---

[hexo hey](https://github.com/nihgwu/hexo-hey) đơnjản là một cái plugin, zùng quảntrị nộizung hexo site. Thằng nào zùng quen hexo rồi không cần đến.

Tao ghi lại đây để tự nhắc mình. Tao bỏ bê hexo thời jan zài, mọi thứ trở lại như mới, gần như fải lần mò từ đầu. Thời jan tìm hiểu, cho zù rút ngắn đôi chút, cũng không thể quenthuộc ngay. Trong lúc lúng túng, **hexo hey** trợ júp tương đối tốt.

Cài đặt và sửzụng không khó, hướngzẫn trên trang của nó đầy đủ rồi. Quảntrị bài viết làm trên browser, zù sao cũng đơn jản và trực quan hơn.

Một tínhnăng hay (không thể không nhắc đến), là ta cothe kéo thả hình ảnh vào bài viết. Nó sẽ tự động upload vào thư mục tương ứng. Làm bài offline cũng tiếtkiệm khá thời jan. Cái này cách quảntrị bằng commandline và text editor không làm được.

---

Một thôngtin ngoài lề: [Microsoft đã mua lại Github](https://blog.github.com/2018-06-04-github-microsoft/). Nói chung chả liêncan deoji đến một cánhân mà webdev chỉ là một hobby như tao. Ngoài github tao còn zùng cả [gitlab](https://about.gitlab.com/) và [bitbucket](https://www.atlassian.com/software/bitbucket). Những tính năng mạnh mẽ thực sự của những công cụ này, tao còn chưa zùng hết, thậm chí nhiều cái còn đéo hiểu cackjisat. Nhưng đối với cộng đồng software developer, hẳn đây là tin gây chấn động không nhỏ.
