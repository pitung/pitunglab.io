---
title: kho hàng online?
tags: [business, life, money, automation]
refs:
  - title: 11 way to automate proceses in e-commerce business
    link: https://blog.asmallorange.com/2016/08/11-ways-automate-processes-ecommerce-business/
    from: asmallorange
  - title: automate selling machine
    link: https://www.act.com/media/en-us/docs/wp/The_Automated_Selling_Machine.pdf
    from: pdf file

date: 2018-08-06 16:15:04
summary: Vài hình dung về tự động hóa quy trình
---

Đầu óc như thùng nước gạo, điếu nghĩ được cái gì cho ra hồn.

Đây không fải vấn đề bán hàng online như thế nào. Mà là việc mô tả quy trình tự động hóa để mọi người trong team, trong thời gian nhanh nhất, hình dung được một cách tổng quát về tình hình buôn bán, đương nhiên ở mỗi vị trí sẽ tiếp cận loại dữ liệu khác nhau.

Google mấy từ khóa quan tâm. Phần lớn toàn quảng cáo và link tới các trang bán hàng. Cả ngày không tìm được bài nào **thực sự** có thông tin. Cách làm SEO của dân VN quả thật gây khó chịu không nhỏ. Toàn khôn vặt. Cứ bảo sao thế giới nó ghét.

Mới phác qua cái quy trình, đã thấy tắc tị mẹ nó rồi

![](https://res.cloudinary.com/bidong/image/upload/v1533702360/pitung.gitlab.io/0806/business_modeling_and_its_problem.jpg)