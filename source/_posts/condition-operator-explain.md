---
title: "jảithích cơbản về hàm điềukiện"
date: 2017-12-20 13:31:48
tags: [code, javascript, operator]
---

https://codeburst.io/javascript-the-conditional-ternary-operator-explained-cac7218beeff

Hàm diềukiện (condition operator) là thứ thường zùng nhât trong lậptrình. Bọn coder trình cao viêt khó hiểu vãilìn. Chủ yếu zo chúng tìm cach viêt tăt cho ngắn bơt.

Bọn boong-thấp viêt như nài:

```javascript
if (person.age >= 16) {
  person.driver = 'Yes';
} else {
  person.driver = 'No';
}
```

Bọn boong-cao viêt như nài:

```javascript
person.driver = person.age >=16 ? 'Yes' : 'No';
```

