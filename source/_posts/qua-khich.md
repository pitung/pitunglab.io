---
title: quá khích?
date: 2018-06-11 16:11:30
tags:
  - think
  - political
---

Đéo ưa chínhtrị, nhưng thấy bàcon ầm ĩ quá. Đành ghé mắt zòm.

Bàcon đang cóvẻ bị kíchđộng mạnh, bởi những thứ **đéo fải quyềnlợi sát sườn**, như vẫn. Nổi cộm là vụ fảnđối chínhfủ thôngqua [zựthảo luật đặckhu kinhtế](http://duthaoonline.quochoi.vn/DuThao/Lists/DT_DUTHAO_LUAT/View_Detail.aspx?ItemID=1319&LanID=1496&TabIndex=1) *(cái này bị bàcon thổi lên thành luật bán nước, thật vãi đái với trí tưởngtượng của bàcon)*, rải rác có người quantâm [zựthảo luật an ninh mạng](http://duthaoonline.quochoi.vn/DuThao/Lists/DT_DUTHAO_LUAT/View_Detail.aspx?ItemID=1382&LanID=1516&TabIndex=1).

Tụtập biểutình và có những hànhđộng quákhích, là hệquả của những bấtcông bàcon đã và đang chịuđựng. Đéo cần jảithích zàizòng, chỉ cần mớm cho bàcon nguycơ "bị cướp đất" là đủ để bàcon nổi con mẹ nó khùng lên rồi.

Cá nhân tao thấy vụ _luật đặc khu_ chỉ là cái mồi chú fỉnh quăng ra cho bà con đớp. Biểutình hay không, cái luật đó cũng đéo thể thông qua ngày một ngày hai. Cânđối quyềnlợi của các nhóm lợi ích trong nộicác là câu chuyện zài, rât zài.

Miếng mồi quá béo. Fe đốilập đã đớp một cách chóngvánh. Xét lýzo, vụ _luật đặckhu_ rõ ràng dễ làm bàcon kíchđộng hơn nhiều. Đẩy một cuộc biểutình nên bạoloạn, thếlực đốilập đang lợizụng tâmlý "ái quốc" của bàcon vô cùng tốt. Tao khen.

Bàcon đéo ngu, nhưng đã quá hâptâp khi coi thường thủ đoạn của chú fỉnh. Trong khi bà con trăntrở lo vận nước, cái _luật an ninh mạng_ ban ra mới **thực sự là con zao thiến jái bàcon**.

Cái chú fỉnh cần, chính là **lýzo để thông qua** đạo luật này. Những "kinh nghiệm đau xót" tung ra trên truyền thông mấy ngày nay cho thấy điều đó. Zăm con xe côngvụ bị đốt, vài trụsở côngquyền bị đập. Cái já quá rẻ để đường đường chính chính nắm quyền thaokhống thôngtin mạng vào tay.

Trích điều 4, mục 2 (nguyên tắc bảo vệ an ninh mạng): "Đặt dưới sự lãnh đạo của Đảng Cộng sản Việt Nam, sự quản lý thống nhất của Nhà nước; huy động sức mạnh tổng hợp của hệ thống chính trị và toàn dân tộc; phát huy vai trò nòng cốt của lực lượng chuyên trách bảo vệ an ninh mạng."

Mưumô cỡ này, thủđoạn cỡ này. Hoàntoàn đủ tầm cỡ đưa vào lịchsử jáokhoa thư. Tao thực lòng khâmfục.

Với chú fỉnh, đó là thắnglợi jònzã. Nhưng với xãhội, nó là nguycơ. Sự lạc-hậu và fảnđộng về tưtưởng, đối với zân VN, e rằng khó tránh.

Cùng với sự fáttriển của mạng-xãhội, bàcon ngày càng hiểu biết. Thôngtin đachiều khiến cho mớ **ngụybiện jáođiều** của chú fỉnh không còn được tintưởng anymore. Những vụ bạođộng mang màu sắc chínhtrị ngày càng quyếtliệt. Nếu tìnhtrạng thôngtin bátnháo (như hiện đang) vẫn tiếpziễn trong vòng 5 năm tới, khả năng zẫnzắt thần zân _"đi từ hết thắng lợi này đến thắng lợi khác"_ của chú fỉnh, là rất thấp.

_Zựthảo luật an ninh mạng_ ra đời, nhằm biến VN thành một fáođài khảzĩ **chống lại mọi cuộc xâmlăng** của tưtưởng tiếnbộ. Đó là jảifáp bắtbuộc, nhưng nó cũng cho thấy sự lúngtúng của chú fỉnh trong điềuhành tưtưởng zânjan. 

Tạisao lúngtúng? Đơnjản bởi vì **sự thật**. Những tưtưởng jáođiều, ngụybiện mà chú fỉnh đang bấu víu, chỉ là lớp vỏ hàonhoáng, để che đậy sự thật bẩn thỉu bên trong. Tao coi **đó là sự zốitrá**. Trong một xãhội mà những tưtưởng trái chiều được tựzo va đập, sự zốitrá sớm muộn jì cũng bị mài chết.

Chú fỉnh đéo chấpnhận điều đó, nhưng cũng đéo đủ zũngkhí quyếtliệt một fen sốngcòn. Mặt nạ đeo quá lâu, nào zễ vuốt xuống ngày một ngày hai. 

Theo cảmnhận cánhân tao, chỉ cần chú fỉnh canđảm thừanhận sai lầm quákhứ, đồngthời thểhiện bảnchất độctài của mình, mọi chuyện sẽ lại đâu vào đấy. Zân VN vốn ngoan như cún, ăn vài roi là yên. Món "dân chủ" vẫn là thứ mà *bản chất zuy tình* của zân VN chưa thể tiêu hóa.

Sự độctài, zĩnhiên không zễ chịu. Nhưng zù sao nó cũng đỡ nguy hại cho jốngnòi hơn sự zốitrá.

---

Bonus quả clip anh tướng fátbiểu "zịchchuyển đám mây ảo về VN là hoàntoàn khả thi"

{% youtube m0pCQP9Mf4o %}

https://www.youtube.com/watch?v=m0pCQP9Mf4o