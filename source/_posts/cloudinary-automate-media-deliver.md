---
title: cloudinary - automate media deliver
tags: [media, cloud, service]
refs:
  - title: cloudinary solution
    link: https://cloudinary.com/documentation/solution_overview
    from: cloudinary documentation
  - title: what is CDN
    link: https://www.incapsula.com/cdn-guide/what-is-cdn-how-it-works.html
    from: incapsula.com
  - title: cookbook
    link: https://cloudinary.com/cookbook
    from: cloudinary

date: 2018-07-31 10:55:13
summary:
---

Đánh site, bất kể tạng gì, hình ảnh là món không thể thiếu. Cloudinary không chỉ là chỗ để upload và share hình ảnh. Nó là giải pháp cho vấn đề lưu trữ và phân phối hình ảnh khắp nhân gian.

Xem khái niệm [content delivery network (CDN)](https://www.incapsula.com/cdn-guide/what-is-cdn-how-it-works.html).

**relate: ** {% post_link cloudinary-media-management-platform "old post about cloudinary on this site" %}

---

### service architecture

![](https://res.cloudinary.com/demo/image/upload/service_architecture.jpg)

### key benefit

- URL-based transformations
- integration with your development framework
- Upload images and videos with a single line of code
- Fetch and manipulate images and videos on-the-fly
- Real-time video manipulation and web optimization
- Powerful management UI and API

---

### [giá cả](https://cloudinary.com/pricing) và [cách dùng](https://cloudinary.com/cookbook)

Tài khoản miễn phí được cấp cho **1 người dùng** với 10GB lưu trữ và 20GB bandwidth. Thừa thãi với quy mô site của doanh nghiệp nhỏ ở VN.

### đăng ký

Bấm đây:

![](https://res.cloudinary.com/bidong/image/upload/v1533011713/pitung.gitlab.io/0731/Image_and_Video_Upload_Storage_Optimization_and_CDN.png)

Rồi điền thông tin vào bảng này (các mục ghi *optional* có thể bỏ qua)

![](https://res.cloudinary.com/bidong/image/upload/v1533011712/pitung.gitlab.io/0731/Cloudinary_Account_Sign_Up.png)

Đăng nhập email bạn đã cung cấp để xác nhận, là xong.

### tải lên (web)

Login (đăng nhập) rồi vào Media Library để tải lên.

![](https://res.cloudinary.com/bidong/image/upload/v1533012336/Desktop_screenshot_zaq6m7.png)

Chọn nguồn (ảnh/video)

![](https://res.cloudinary.com/bidong/image/upload/v1533012696/pitung.gitlab.io/0731/Cloudinary_Management_Console_Media_Library.png)

- **My Files** -- từ computer
- **Web Adress** -- link trên mạng
- **Image Search** -- tìm trên google
- **Dropbox** -- từ tài khoản dropbox
- **Facebook** -- từ tài khoản facbook
- **Instagram** -- từ tài khoản instagram

Thế thôi đã.

---

ps. Sức mạn thực sự của cloudinary là [ở chỗ này](https://cloudinary.com/documentation/image_upload_api_reference). Nhưng đó là vấn đề của những gã làm kỹ thuật. Sẽ buôn tiếp về vấn đề này trong một bài khác nếu có dịp.