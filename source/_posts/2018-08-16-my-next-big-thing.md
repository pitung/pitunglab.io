---
title: my next big thing
tags: [vue, shop, webdev]
refs:
  - title: install vue storefront and integrate with magento2
    link: https://medium.com/@piotrkarwatka/vue-storefront-how-to-install-and-integrate-with-magento2-227767dd65b2
    from: medium

date: 2018-08-16 10:07:25
summary: Quá nhiều thứ phức tạp cần tìm hiểu.
---

Dang qwantam vande suzung vue lam onlineshop. Canhan tao camthay vue fuhop voi bon tayngang (amateur), bon taychuyen (profesional) thuong zung react hoac angular. Vande cua viec suzung hai thang do, la fai thucsu amhieu kienthuc coban.

Dayla [google search trending](https://trends.google.com/trends/explore?geo=VN&q=vue,react,angular) tai Vietnam. Screen capture at this moment.

![](https://res.cloudinary.com/bidong/image/upload/v1534391631/pitung.gitlab.io/0816/vue_react_angular_Explore_Google_Trends.png)

Bieudo [global trend](https://trends.google.com/trends/explore?q=vue,react,angular), soluong timkiem "vue" keyword nhieuhon.

![](https://res.cloudinary.com/bidong/image/upload/v1534391982/pitung.gitlab.io/0816/vra.png)

---

**Angular** is the best choice for *large-scale projects*, **React** is ideal for those who *do not want to limit* themselves to any frames, and **Vue** is a kind of “compromise” between these two solutions and is often chosen for *work in small teams*.

---

Chozu nhanxet, danhja kieu ji, google may keyword tang *"react vs vue vs angular..."* (or the likes), dambao bacon se loan me doulou. Moi thang danhja mot kieu. Cac danhja do co khachqwan hay khong? Deofai viec tao quwantam rightnow.

Cai tao dang qwantam, la ong nay: [VUE STOREFRONT](https://www.vuestorefront.io/)

**Wtf is it?**

Vue Storefront is a **standalone PWA storefront** for your eCommerce, able to connect with **any eCommerce backend** (eg. Magento, Prestashop or Shopware) through the API. Vue Storefront is, and *always* will be, **open source**. Anyone can use and support the project. We want it to be a tool for the improvement of the shopping experience.

**Is that interesting?**

![](https://user-images.githubusercontent.com/18116406/33559277-920deb00-d90c-11e7-95e6-ffd55a36a5c6.gif)

![](https://user-images.githubusercontent.com/18116406/33560504-3105dac6-d910-11e7-847d-70ef8e944321.gif)

Tatnhien cung jongnhu ratnhieu tutorial khac, lam theo huongzan khongbaojo thanhcong ngay tu buoc doutien.

---

Dungnhu daluongtruoc. Sau 1 buoitoi caidat tren Mac va nua ngay nua tren PC. Cahai deu tacti sau vai buoc. Khongthe di tiep, vi deobiet duongnao ma di. Rat bucminh, nhung cung phai tutu timhieu thoivay.