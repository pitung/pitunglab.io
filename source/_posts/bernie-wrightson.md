---
title: frankenstein comic book
type: collection
category: collection
tags:
  - ink
  - art
  - comic
preview: 'http://4.bp.blogspot.com/_R7WHoDYUAhw/TMmpsYWO7AI/AAAAAAAAAPU/zO4NFvTKglk/s1600/Bernie+Wrightson+-+Frankentien+41.jpg'
summary: America ink and brush illustrator
artist: bernie wrightson
linkto: https://en.wikipedia.org/wiki/Bernie_Wrightson
date: 2018-07-29 17:43:17
---

[Frankenstein comic book](http://perdidoemrabiscos.blogspot.com/2010/10/bernie-wrightson-frankenstein.html)

![](http://2.bp.blogspot.com/_R7WHoDYUAhw/TMmVaxkG9lI/AAAAAAAAANQ/GWXy8Ax9UZM/s1600/Bernie+Wrightson+-+Frankentien+02.jpg)

![](http://1.bp.blogspot.com/_R7WHoDYUAhw/TMmVL-EudqI/AAAAAAAAANM/_1gxC_yye0g/s1600/Bernie+Wrightson+-+Frankentien+01.jpg)

![](http://4.bp.blogspot.com/_R7WHoDYUAhw/TMmUrYamolI/AAAAAAAAANE/rBFAFClgNh4/s1600/Bernie+Wrightson+-+Frankentien+05.jpg)

![](http://2.bp.blogspot.com/_R7WHoDYUAhw/TMmVzEoyAFI/AAAAAAAAANU/tBZZ07orK5w/s1600/Bernie+Wrightson+-+Frankentien+03.jpg)

![](http://3.bp.blogspot.com/_R7WHoDYUAhw/TMmWBSB3UsI/AAAAAAAAANY/3rV9Dx28JWg/s1600/Bernie+Wrightson+-+Frankentien+04.jpg)

![](http://3.bp.blogspot.com/_R7WHoDYUAhw/TMmXG1ko17I/AAAAAAAAANo/OQbMzy3Kld8/s1600/Bernie+Wrightson+-+Frankentien+08.jpg)

![](http://4.bp.blogspot.com/_R7WHoDYUAhw/TMmWtUWDNcI/AAAAAAAAANg/nchGNFHwk_Y/s1600/Bernie+Wrightson+-+Frankentien+06.jpg)

![](http://1.bp.blogspot.com/_R7WHoDYUAhw/TMmW4YEcz6I/AAAAAAAAANk/FKaWQgR_GIw/s1600/Bernie+Wrightson+-+Frankentien+07.jpg)

![](http://4.bp.blogspot.com/_R7WHoDYUAhw/TMmWcXTykfI/AAAAAAAAANc/K4gdXQq7TgY/s1600/Bernie+Wrightson+-+Frankentien+11.jpg)

![](http://3.bp.blogspot.com/_R7WHoDYUAhw/TMmXXKWl5zI/AAAAAAAAANs/yG0v_zKwdlU/s1600/Bernie+Wrightson+-+Frankentien+09.jpg)

![](http://3.bp.blogspot.com/_R7WHoDYUAhw/TMmXgpTv6MI/AAAAAAAAANw/-lNOYdKQozY/s1600/Bernie+Wrightson+-+Frankentien+10.jpg)

![](http://2.bp.blogspot.com/_R7WHoDYUAhw/TMmZCjtQIXI/AAAAAAAAAOA/1fL34G3YwLM/s1600/Bernie+Wrightson+-+Frankentien+14.jpg)

![](http://2.bp.blogspot.com/_R7WHoDYUAhw/TMmYiLYDHaI/AAAAAAAAAN4/eikioPZc2vM/s1600/Bernie+Wrightson+-+Frankentien+12.jpg)

![](http://4.bp.blogspot.com/_R7WHoDYUAhw/TMmYx8qUCpI/AAAAAAAAAN8/cFV-N6lMgl4/s1600/Bernie+Wrightson+-+Frankentien+13.jpg)

![](http://1.bp.blogspot.com/_R7WHoDYUAhw/TMmYNn7VFmI/AAAAAAAAAN0/LTPmwn--gxw/s1600/Bernie+Wrightson+-+Frankentien+17.jpg)

![](http://1.bp.blogspot.com/_R7WHoDYUAhw/TMmZUpOAE4I/AAAAAAAAAOE/LiWrl-fsYnQ/s1600/Bernie+Wrightson+-+Frankentien+15.jpg)

![](http://1.bp.blogspot.com/_R7WHoDYUAhw/TMmZfdwhz2I/AAAAAAAAAOI/vpWaImuM9ZI/s1600/Bernie+Wrightson+-+Frankentien+16.jpg)

![](http://3.bp.blogspot.com/_R7WHoDYUAhw/TMmjUyYCtXI/AAAAAAAAAOY/6Bms2tuDHQo/s1600/Bernie+Wrightson+-+Frankentien+20.jpg)

![](http://2.bp.blogspot.com/_R7WHoDYUAhw/TMmhgCHDstI/AAAAAAAAAOQ/mQ72xb88Flk/s1600/Bernie+Wrightson+-+Frankentien+18.jpg)

![](http://4.bp.blogspot.com/_R7WHoDYUAhw/TMmi_CH2sII/AAAAAAAAAOU/UfYumcClajU/s1600/Bernie+Wrightson+-+Frankentien+19.jpg)

![](http://1.bp.blogspot.com/_R7WHoDYUAhw/TMmhT4jQ3PI/AAAAAAAAAOM/B4x8R7jHlVs/s1600/Bernie+Wrightson+-+Frankentien+23.jpg)

![](http://1.bp.blogspot.com/_R7WHoDYUAhw/TMmjfidNBOI/AAAAAAAAAOc/PANYgIFGntA/s1600/Bernie+Wrightson+-+Frankentien+21.jpg)

![](http://4.bp.blogspot.com/_R7WHoDYUAhw/TMmj3H5laYI/AAAAAAAAAOg/X0exprK302I/s1600/Bernie+Wrightson+-+Frankentien+22.jpg)

![](http://4.bp.blogspot.com/_R7WHoDYUAhw/TMmj3H5laYI/AAAAAAAAAOg/X0exprK302I/s1600/Bernie+Wrightson+-+Frankentien+22.jpg)

![](http://3.bp.blogspot.com/_R7WHoDYUAhw/TMmlT23UHgI/AAAAAAAAAOw/MIlWkVUSu-0/s1600/Bernie+Wrightson+-+Frankentien+26.jpg)

![](http://2.bp.blogspot.com/_R7WHoDYUAhw/TMmklz2kDtI/AAAAAAAAAOo/koLKpvNkS4g/s1600/Bernie+Wrightson+-+Frankentien+24.jpg)

![](http://2.bp.blogspot.com/_R7WHoDYUAhw/TMmk19G6bWI/AAAAAAAAAOs/llWCOBD78yM/s1600/Bernie+Wrightson+-+Frankentien+25.jpg)

![](http://3.bp.blogspot.com/_R7WHoDYUAhw/TMmkY_XnPZI/AAAAAAAAAOk/1Bq9qb7BLhg/s1600/Bernie+Wrightson+-+Frankentien+29.jpg)

![](http://3.bp.blogspot.com/_R7WHoDYUAhw/TMmlrAiCO5I/AAAAAAAAAO0/M5RAk6HhJW0/s1600/Bernie+Wrightson+-+Frankentien+27.jpg)

![](http://2.bp.blogspot.com/_R7WHoDYUAhw/TMml6ext79I/AAAAAAAAAO4/brssSj7Yi6k/s1600/Bernie+Wrightson+-+Frankentien+28.jpg)

![](http://1.bp.blogspot.com/_R7WHoDYUAhw/TMmoHUKz0zI/AAAAAAAAAPI/PiuKe3dVo6s/s1600/Bernie+Wrightson+-+Frankentien+32.jpg)

![](http://2.bp.blogspot.com/_R7WHoDYUAhw/TMmncpNtkUI/AAAAAAAAAPA/EXycs5C75fA/s1600/Bernie+Wrightson+-+Frankentien+30.jpg)

![](http://1.bp.blogspot.com/_R7WHoDYUAhw/TMmnwbNO3II/AAAAAAAAAPE/lurcRd5PQoE/s1600/Bernie+Wrightson+-+Frankentien+31.jpg)

![](http://4.bp.blogspot.com/_R7WHoDYUAhw/TMmnItidP2I/AAAAAAAAAO8/trq0diiTvAU/s1600/Bernie+Wrightson+-+Frankentien+35.jpg)

![](http://4.bp.blogspot.com/_R7WHoDYUAhw/TMmoiPdsspI/AAAAAAAAAPM/inb4d_Rft7U/s1600/Bernie+Wrightson+-+Frankentien+33.jpg)

![](http://3.bp.blogspot.com/_R7WHoDYUAhw/TMmpCKgCOYI/AAAAAAAAAPQ/UvbGwg2AWEk/s1600/Bernie+Wrightson+-+Frankentien+34.jpg)

![](http://1.bp.blogspot.com/_R7WHoDYUAhw/TMmqm5mIUiI/AAAAAAAAAPg/n6xvFqBhqmo/s1600/Bernie+Wrightson+-+Frankentien+38.jpg)

![](http://1.bp.blogspot.com/_R7WHoDYUAhw/TMmqFKBKBBI/AAAAAAAAAPY/7k_g_V4i8lM/s1600/Bernie+Wrightson+-+Frankentien+36.jpg)

![](http://1.bp.blogspot.com/_R7WHoDYUAhw/TMmqPiogTdI/AAAAAAAAAPc/5Vj48Lbkf8s/s1600/Bernie+Wrightson+-+Frankentien+37.jpg)

![](http://4.bp.blogspot.com/_R7WHoDYUAhw/TMmpsYWO7AI/AAAAAAAAAPU/zO4NFvTKglk/s1600/Bernie+Wrightson+-+Frankentien+41.jpg)

![](http://2.bp.blogspot.com/_R7WHoDYUAhw/TMmrSnFOLEI/AAAAAAAAAPk/LYJa7q3JFug/s1600/Bernie+Wrightson+-+Frankentien+39.jpg)

![](http://3.bp.blogspot.com/_R7WHoDYUAhw/TMmrnBF_LqI/AAAAAAAAAPo/-FG64Y1b5hs/s1600/Bernie+Wrightson+-+Frankentien+40.jpg)

![](http://3.bp.blogspot.com/_R7WHoDYUAhw/TMm2dTkgGSI/AAAAAAAAAP4/PTa3jqvOjDM/s1600/Bernie+Wrightson+-+Frankentien+44.jpg)

![](http://3.bp.blogspot.com/_R7WHoDYUAhw/TMm1jpgczNI/AAAAAAAAAPw/Zg8aQZxi2wo/s1600/Bernie+Wrightson+-+Frankentien+42.jpg)

![](http://4.bp.blogspot.com/_R7WHoDYUAhw/TMm13w7NeCI/AAAAAAAAAP0/EC_EdmPiBmc/s1600/Bernie+Wrightson+-+Frankentien+43.jpg)

![](http://3.bp.blogspot.com/_R7WHoDYUAhw/TMm2taaQOrI/AAAAAAAAAP8/S1OSvRz3Aj0/s1600/Bernie+Wrightson+-+Frankentien+45.jpg)

![](http://3.bp.blogspot.com/_R7WHoDYUAhw/TMm1QqxdDlI/AAAAAAAAAPs/DUu368N1INk/s1600/Bernie+Wrightson+-+Frankentien+46.gif)

