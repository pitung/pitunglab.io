---
title: xế điện
tags: [news, electic, car]
date: 2018-10-15
summary: bao giờ xe diện mới phổ biến?
---

Đọc báo thấy bọn tay chuyên dùng con số thống kê rất thú vị. Dễ hình dung, lại tiện quy đổi thành tiền ở mọi nơi: “Nghiên kíu chế tạo một ông xe điện tiêu tốn **100 triệu mã lực**, gấp 5-6 lần làm ra ông tàu bay” ([vietnamnet](http://vietnamnet.vn/vn/chuyen-trang/oto-xemay/xe-dien-ngoi-sao-tuong-lai-481248.html)).

Khi một thứ gì đó phổ biến trong nhân gian đủ lâu, hay giá thành đủ rẻ, thiên hạ sẽ mất đi khái niệm cuộc sống của mình đang lệ thuộc vào nó. Điện, nước sạch, thậm chí mạng internet ở các thành thị là ví dụ.

Mất điện một ngày, thậm chí một tuần, sanh hoạt nhân dân nhẽ không vấn đề gì quá lớn. Nhưng thay điện bằng hehe nước thử xem. Cuộc sống đô thị đảm bảo náo loạn. Thiệt hại sẽ là không thể đo đếm.

hôm trc đọc bài báo chỉ riêng việc đái ỉa bậy của nhân dân cũng dẫn đến thiệt hại 16 tỉ cụ hàng năm lên ngân sách nhà nước. Đừng đùa với những thứ tưởng chừng đơn giản. Hãy suy rộng ra với những hành vi xấu khác như hút thuốc lá, uống rượu bia, vượt đèn đỏ, tắc đường, rác thải, vv... Tất cả những thứ ý, đều làm phát sinh gánh nặng chi tiêu lên chính quyền. Chúng thu thuế của batoong, là nhằm giải quyết những thứ vớ vẩn ý, chứ có cặc mà đủ để làm những việc to tát khác.

cũng báo chí, [9000 tấn rác thải mỗi ngày](https://m.baomoi.com/cau-chuyen-rac-thai-tai-tp-hcm-moi-ngay-co-9-000-9-500-tan-rac-thai/c/27818054.epi) là khối lượng dân tphcm thải ra. Một con số khủng khiếp. Gúc tiếp số liệu trên cả nước, con số còn khủng hơn nữa.

---

vài link quan tâm

- https://blog.cloudflare.com/fast-google-fonts-with-cloudflare-workers/ — load google font nhanh hơn?
- https://bitsofco.de/what-exactly-is-the-dom/ — DOM aka document object model là kiến thức cơ bản nhất phải đọc để làm web, trước giờ lười không chịu đọc.