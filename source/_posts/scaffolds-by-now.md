---
title: scaffolds by now
tags: [hexo, webdev]
date: 2018-08-02 13:32:07
summary: lưu tạm đây cho khỏi quên
refs:
  - title: hexo scaffolds
    link: https://hexo.io/docs/writing#Scaffolds
    from: hexo.io
---

I created 3 post type for this site.

## use

`cd` into site folder

### create

- normal post (blog post): `hexo new "blog post title"`
- selling post: `hexo new selling "selling post title"`
- collection post: `hexo new collection "normally artist name"`

---

### default post

```yaml
---
title: {{ title }}
date: {{ date }}
tags: [tag, tag, tag]
# replace preview image
preview: 'https://source.unsplash.com/iFSvn82XfGo/700x500'
summary: null
refs:
  - title: null
    link: null
    from: null
---
```

### selling post

```yaml
---
title: {{ title }}
date: {{ date }}
type: {{ layout }}
category: {{ layout }}
tags: [tag, tag, tag]
# replace preview image
preview: 'https://source.unsplash.com/iFSvn82XfGo/700x500'
summary: null
# product data
code: null
old_price: null
price: null
# product feature
overview:
  - list feature
  - of product
  - made to order
shipping:
  - ready to ship
  - deliver as payment
# more product image (image url, optional)
images:
  - null
  - null
  - null 
---
```

### collection post

```yaml
---
title: {{ title }}
date: {{ date }}
type: {{ layout }}
category: {{ layout }}
tags: [tag, tag, tag]
# replace preview image
preview: 'https://source.unsplash.com/IQIkl2iGnbw/700x500'
summary: null
# artist
artist: {{ title }}
linkto: '/link/to/artist/site'
# more photo (image url, optional)
collection:
  - null
  - null
  - null
---
```
