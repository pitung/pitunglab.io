---
title: "kết nối api với javascript"
date: 2017-12-20 12:31:12
tags: [code, javascript, api]
---

Hướngzẫn tạo một trang web dơnjản hiểnthị thôngtin lấy từ API của một trang khác, zĩnyen bằng javascript.

https://www.taniarascia.com/how-to-connect-to-an-api-with-javascript/

Dọc một lần không hiểu thì hai lần, vẫn không hiểu thì đọc tiếp, tiếp nữa...

### index.html file

```html
<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>Test App</title>
</head>

<body>
  <div id="root"></div>
  <script src="scripts.js"></script>
</body>

</html>
```

### script.js file

```javascript
const app = document.getElementById('root');

const logo = document.createElement('img');
logo.src = 'logo.png';

const container = document.createElement('div');
container.setAttribute('class', 'container');

app.appendChild(logo);
app.appendChild(container);

var request = new XMLHttpRequest();
request.open('GET', 'https://ghibliapi.herokuapp.com/films', true);
request.onload = function () {

  // Begin accessing JSON data here
  var data = JSON.parse(this.response);
  if (request.status >= 200 && request.status < 400) {
    data.forEach(movie => {
      const card = document.createElement('div');
      card.setAttribute('class', 'card');

      const h1 = document.createElement('h1');
      h1.textContent = movie.title;

      const p = document.createElement('p');
      movie.description = movie.description.substring(0, 300);
      p.textContent = `${movie.description}...`;

      container.appendChild(card);
      card.appendChild(h1);
      card.appendChild(p);
    });
  } else {
    const errorMessage = document.createElement('marquee');
    errorMessage.textContent = `Gah, it's not working!`;
    app.appendChild(errorMessage);
  }
}

request.send();
```