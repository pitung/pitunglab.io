---
title: nhặt nhạnh và so sánh mấy kiểu bày hàng chợ
tags: [style, design, research]
date: 2018-08-12 17:07:12
summary: Hầu hết các trang thương mại điện tử lớn đều có cùng kiểu trình bày.
refs:
  - title: Improve shopping experience
    link: https://ecommerceuxdesign.com/
    from: ecommerceuxdesign
---

Một sản phẩm lên chợ, cần những thông tin gì? Login vào một trang quản trị rồi ngồi lẩn mẩn điền form thực sự là công việc nhàm chán. Có cách nào thú vị hơn để làm những việc đó không?

Hàng bày trên ebay. [full page here](https://res.cloudinary.com/bidong/image/upload/v1534068987/pitung.gitlab.io/0812/ebayfull.jpg)

![](https://res.cloudinary.com/bidong/image/upload/v1534069499/pitung.gitlab.io/0812/ebaycrop.jpg)

Hàng bày trên Amazon. [full page here](https://res.cloudinary.com/bidong/image/upload/v1534068930/pitung.gitlab.io/0812/amazonfull.jpg)

![](https://res.cloudinary.com/bidong/image/upload/v1534068914/pitung.gitlab.io/0812/amazoncrop.jpg)

Hàng bày trên aliexpress. [full page here](https://res.cloudinary.com/bidong/image/upload/v1534068930/pitung.gitlab.io/0812/alifull.jpg)

![](https://res.cloudinary.com/bidong/image/upload/v1534068836/pitung.gitlab.io/0812/alicrop.jpg)

Hàng bày trên sendo. [full page here](https://res.cloudinary.com/bidong/image/upload/v1534068908/pitung.gitlab.io/0812/sendofull.jpg)

![](https://res.cloudinary.com/bidong/image/upload/v1534068838/pitung.gitlab.io/0812/sendocrop.jpg)

Hàng bày trên lazada. [full page here](https://res.cloudinary.com/bidong/image/upload/v1534068914/pitung.gitlab.io/0812/lazadafull.jpg)

![](https://res.cloudinary.com/bidong/image/upload/v1534068887/pitung.gitlab.io/0812/lazadacrop.jpg)

Có thể thấy các trang thương mại điện tử lớn đều dùng chung kiểu sắp xếp. Hoàn toàn không phải do đẹp hay không đẹp, thích hay không thích. Cách sắp xếp như thế đã được nghiên cứu cẩn thận. Thói quen của người dùng là ưu tiên số 1 của họ. Làm khác đi là ăn đòn lập tức, đéo đùa được đâu.

Cùng kiểu sắp xếp, cá nhân tao thiện cảm với etsy hơn cả, mặc dù nó không "lổi" như mấy thằng trên kia. Etsy định vị nó là trang chuyên bán hàng chất nghệ, nên hình thức được đầu tư rất kỹ. Dù vẫn là chợ, nhưng nó gần với các shop chuyên dụng rồi.

Hàng bày trên etsy. [full page here](https://res.cloudinary.com/bidong/image/upload/v1534068882/pitung.gitlab.io/0812/etsyfull.jpg)

![](https://res.cloudinary.com/bidong/image/upload/v1534068909/pitung.gitlab.io/0812/etsycrop.jpg)

Lên chợ rõ ràng không tiện nghi như shop chuyên dụng. Trải nghiệm mua sắm cũng tệ hơn nhiều. Thế nhưng nhiều shop chuyên dụng lại lấy hình mẫu của kiểu bày hàng này, bắt designer làm giống thế. Kết quả: *lượng thông tin không đủ, hàng bày quá ít, nội dung khấp khểnh cái đủ cái không* khiến trang web nom nghèo nàn một cách thảm hại. 

Họ hoàn toàn có thể làm tốt hơn thế. Sản phẩm của họ tốt, thậm chí rất tốt, người bán thân thiện, dịch vụ sau bán hàng chu đáo. Nhưng xem cách họ bày trên web thực sự chỉ muốn huỷ mẹ đơn hàng. Vấn đề ở đâu?

Các trang web giới thiệu sản phẩm ở VN, ngay cả khi họ thực sự nghiêm túc với sản phẩm của mình, cũng thường chỉ bày vài cái ảnh méo mó, chụp vội bằng điện thoại (hoặc luộc lại đâu đó), với đôi ba dòng mô tả sơ sài copy từ Google Translate. Đương nhiên **giá: liên hệ** là món không thể thiếu. Tao cứ nghĩ mãi, đéo hiểu bọn nó cứ phải tương cái dòng đấy lên làm gì. Thôi tạm bằng lòng với giải thích là theo thói quen đi. Kiểu cứ phải cộng hoà xã hội vân vân các cái khi viết kiểm điểm. Nó chỉ gây khó chịu. Chả ai chết vì khó chịu bao giờ.

Hãy xem [moleskine bán sổ](https://us.moleskine.com/en/), [misumi bán linh kiện cơ điện](https://vn.misumi-ec.com/), [ikea bán đồ nội thất](https://www.ikea.com/), [infinity firenze bán đồ da](https://www.infinityfirenze.com/), v..v... Có cả ngàn cái ví dụ như thế trên web.

Cũng là mua quần sịp, có người thích tặc lưỡi mua ở chợ, có người thích vào siêu thị nhặt đồ, nhưng cũng có người phải vào shop đặt may đo. Tính năng vẫn thế, chất liệu vẫn thế, nhưng đẳng cấp khác biệt nó thể hiện ở đấy. 

Dựng shop rồi lại đi biến nó thành chợ. Vậy đừng hỏi tại sao khách khứa phàn nàn giá của bạn không rẻ như của dân bán buôn.

## bonus

Vài nguyên tắc khi list sản phẩm lên web:

Quy định các trường thông tin **bắt buộc phải có** với mỗi sản phẩm. Quy định cách hiển thị những thông tin này trên các loại màn hình khác nhau. Nếu là chữ, xác định số chữ trung bình, ít nhất, nhiều nhất. Thử với mọi trường hợp có thể xảy ra.

Xác định *lượng thông tin phụ* tham gia vào việc mô tả sản phẩm và vị trí của chúng trong thiết kế. Đặt câu hỏi liên tục: "Nếu không có thông tin này, thì cái gì sẽ xuất hiện ở đó?" với mỗi thông tin.

Mỗi dòng sản phẩm áp dụng một kiểu thông tin mô tả riêng. Rõ ràng không thể áp dụng form trình bày hàng nông sản cho máy bơm nước. Hãy đảm bảo mỗi dòng sản phẩm đều đủ thông tin cần thiết.

Thuê designer làm hình ảnh, nếu có thể.