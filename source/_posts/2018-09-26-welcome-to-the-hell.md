---
title: xảy chân rơi vào địa ngục
tags: [emacs, editor, programing]
date: 2018-09-26
summary: Trước giờ vẫn tránh dùng Vim hết sức có thể. Thế điếu nầu xảy chân vồ vào Emacs. Đúng là ghét của nào trời trao của ấy.
preview: https://thumbs-prod.si-cdn.com/ugZIDWrhD91nVJldCuv8wQFmieU=/800x600/filters:no_upscale()/https://public-media.smithsonianmag.com/filer/5c/35/5c35868b-3f81-4500-9b40-2a71b4d21bed/42-49412163.jpg
---

Welcome to the REAL hell

![](https://arisuchan.jp/%CE%BB/src/1496425603371-0.png)

Lạc vào thế giới Emacs rồi mới thấy đây mới thực sự là địa ngục. So với nó, những khó chịu với webdev trước đây chả là điếu jisât.

Emacs nhiều tính năng, dễ làm việc, dễ config, dễ các thứ? Bà con thấy thằng nào phát ngôn câu ấy cứ bắn bỏ mẹ nó đi. Có zùng emacs mới thấy vai trò quan trọng của Bill Gates và Steve Jobs đối với sự phát triển của nhân loại. Không có mấy anh ý, nhẽ giờ này phần mềm soạn thảo text trên máy tính đang được giảng dạy ở cấp đại học, chưa biết chừng còn là tiêu chuẩn xét tốt nghiệp của sinh viên chứ đéo đùa.

Mà thôi, trót rơi vào rồi thì xông qua thôi. Hmm! địa ngục thì đã làm sao.