---
title: cloudinary - media management platform
date: 2017-12-18 09:47:02
tags: [media, photo]
---

[Cloudinary](https://cloudinary.com/) là chỗ cho luwtrữ media files. 

[Tàikhoản miễnfi](https://cloudinary.com/pricing) cũng thoải mái zùng. 300k file total, 10GB storage, 20GB bandwidth monthly. Nó cungcấp nhiều API [xửlý hìnhảnh](https://href.li/?https://cloudinary.com/cookbook/) hữuzụng với các site tạng [JAMstack](https://href.li/?https://jamstack.org).

Luw lại đây, fòng có lúc zùng tới.

---

Original file

```text
http://res.cloudinary.com/bidong/image/upload/v1513445827/IMG_0919_ybneml.jpg
```

![](http://res.cloudinary.com/bidong/image/upload/v1513445827/IMG_0919_ybneml.jpg)

Convert to BW

```text
http://res.cloudinary.com/bidong/image/upload/v1513445827/e_grayscale/IMG_0919_ybneml.jpg
```

![](http://res.cloudinary.com/bidong/image/upload/v1513445827/e_grayscale/IMG_0919_ybneml.jpg)

Other effect

```text
http://res.cloudinary.com/bidong/image/upload/e_gradient_fade,x_1.0/u_IMG_0919_ybneml,e_grayscale/v1513445827/IMG_0919_ybneml.jpg
```

![](http://res.cloudinary.com/bidong/image/upload/e_gradient_fade,x_1.0/u_IMG_0919_ybneml,e_grayscale/v1513445827/IMG_0919_ybneml.jpg)

Zĩnhiên còn nhiều thứ hayho khác. 

Diểm bâtiện là jaoziện app của nó chỉ hợp khi zùng trên computer. Tablet & smartphone cũng vào được, nhưng ziệntích hiểnthị không đủ zùng.