---
title: code highlight to prism
refs:
  - title: prism syntax highlighter
    link: https://prismjs.com/
    from: prismjs
  - title: top 5 best syntax highlighter javascript plugins
    link: https://ourcodeworld.com/articles/read/140/top-5-best-code-syntax-highlighter-javascript-plugins
    from: ourcodeworld
  - title: converts your code snippets into pretty-printed HTML format
    link: http://hilite.me/
    from: hilite.me

date: 2018-07-28 10:04:04
tags: [code, style, hexo]
---

Đổi sang dùng [prism code highlight](https://prismjs.com/) thay cho [highlightjs](https://highlightjs.org/) mặc định của hexo

```bash
npm install --save hexo-prism-plugin
```

Sửa `_config.yml`

```yaml
highlight:
  enable: false

prism_plugin:
  mode: 'preprocess'
  theme: 'default'
  line_number: true
```

remove in `style.styl`

```stylus
@import "_partial/highlight"
```

Xem thêm đây: https://github.com/ele828/hexo-prism-plugin