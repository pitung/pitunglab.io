---
title: lạc đường cmnr!
tags: [web, idea]
date: 2018-08-20 23:26:31
refs:
  - title: The web I want
    link: https://dev.to/quii/the-web-i-want-43o
    from: Chris James — dev.to
summary: Web đã trở thành một thứ biến thái. Chức năng chính của nó giờ là bán hàng. Bọn dùng web để chia sẻ thông tin, kiến thức chỉ sử dụng hết một góc nhỏ.
---

Đang cảm thấy lạc đường. Những thứ xuất phát từ *tao muốn* đang lấn át và che khuất những cái *tao cần*. Webdev giờ quá nhiều công cụ hay ho. Nó làm tao {% post_link my-next-big-thing "muốn có những trải nghiệm mới" %}. Thực tế cần thiết không? Hỏi cũng là trả lời cbn rồi.

Điều cần thiết ở một trang web là gì?

Chỉ hai chữ thôi — **THẬT NHANH**. Tao cũng là người dùng web bình thường. Cái tao quan tâm là **thông tin nhận được**, chứ công nghệ hay quy trình làm ra nó ưu việt ra sao thì quan tâm làm đéo.

Nhanh ở đây còn có nghĩa rộng hơn chút. Đó là *thời gian* dùng để làm ra nó. Cứ mân mó mãi những *components, functions, methods, variables* các cái, trong khi kiến thức cơ bản trống rỗng, thì đời đéo nào mới làm được một thứ ra hồn? Chả sức đâu mà vận hành hành những thứ phức tạp. Đưa sản phẩm ra thị trường cần thiết hơn.

Lạc đường? Đúng. Nhiều khi tao quên rằng để chiến đấu, thì hiệu quả nhất là *vũ khí thuận tay*, chứ không phải những thứ đèn còi loè loẹt được quảng bá ầm ĩ.

Tao không phủ nhận lợi ích những thứ đó mang lại. Không phải tự nhiên cả thế giới đều đâm đầu vào. Vấn đề là tại thời điểm này, tao chưa đủ trình để hiểu và vận dụng nhuần nhuyễn.

Tóm lại, **cái tao cần lúc này là content**. Tiện gì phang nấy thôi.

Now. Immediately. Quick. Hurry up. Faster.