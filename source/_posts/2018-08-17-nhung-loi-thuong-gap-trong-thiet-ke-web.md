---
title: những lỗi thường gặp trong thiết kế web
tags: [design, style, typo]
refs:
  - title: articles website design mistakes
    link: http://blog-en.tilda.cc/articles-website-design-mistakes
    from: Nikita Obukhov - tilda publishing founder
date: 2018-08-17 09:48:15
summary:
---

Lưu đây thỉnh thoảng xem lại. [link](http://blog-en.tilda.cc/articles-website-design-mistakes)

Đa phần là các lỗi liên quan trình bày bài viết. Xem để biết thế thôi. Làm thiết kế đôi khi cũng nên có những thử nghiệm hmm... dị hợm một chút. Có sao đâu, miễn hợp lý là được.

![](http://www.alisharesidence.com/wp-content/uploads/2018/07/luxury-couch-potato-furniture-40-kingsnorth.jpg)