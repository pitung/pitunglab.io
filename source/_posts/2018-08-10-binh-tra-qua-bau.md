---
title: bình pha trà, bình ủ trà thủy tinh quả bầu có áo giữ nhiệt
type: selling
category: selling
tags: [test, product]
preview: 'https://scdn.thitruongsi.com/image/cached/size/w800-h0/img/product/2017/11/14/f28317e0-c925-11e7-b186-f11bdf948cdc.jpg'
overview:
  - bình thủy tinh 500ml
  - nắp nhựa chống thoát nhiệt
  - áo giữ nhiệt nhiều màu
  - quai xách chắc chắn
shipping:
  - ready to ship
  - deliver as payment
images:
  - https://scdn.thitruongsi.com/image/cached/size/w800-h0/img/product/2017/11/14/f28317e0-c925-11e7-b186-f11bdf948cdc.jpg
date: 2018-08-10 20:51:50
summary: bình trà thuỷ tinh có áo giữ nhiệt màu sắc trẻ trung tươi tắn
code: prd#002
old_price:
price: 62 k
---

![](https://scdn.thitruongsi.com/image/cached/size/w800-h0/img/product/2017/11/14/f28317e0-c925-11e7-b186-f11bdf948cdc.jpg)

Bài này oánh dấu selling post, là bởi vì tao muốn thêm một post nữa hiển thị trên trang selling.

Loay hoay mấy ngày tìm kiếm giải pháp tự động hoá, vẫn chưa đâu vào đâu. Làm theo kiểu thủ công cũng không phải quá khó. Chỉ cần lập bảng dữ liệu (google sheet), download file `.csv` về rồi kiếm chỗ convert nó thành `.json` hoặc `.yaml` (trò convert này gúc phát ra cả đống, ví dụ [ông này](http://www.convertcsv.com/csv-to-yaml.htm)). Tải file đã convert về cho vào `_data` folder là dùng được.

![](https://res.cloudinary.com/bidong/image/upload/v1533911646/pitung.gitlab.io/0806/screencapture-localhost-4000-categories-selling-2018-08-10-21_28_07.png)

Làm kiểu này chỉ phù hợp để layout. Trình bày xong sẽ không có link để mở ra thông tin chi tiết, cũng không thể phân trang, hay chia category, tag...

Lý do tao vẫn cứ bám vào làm static site, là vì tao ưa dùng text editor để soạn thông tin. Dùng CMS kiểu gì cũng phải login và online mới làm được. Lại khó dùng, khó kiểm soát từng bài. Cả một hệ thống cơ sở dữ liệu, nhất là dữ liệu bán hàng, tạng bảng tính với rất nhiều tham số phức tạp, lại đi chia thành static file có dở hơi không chứ?.

Hexo không đáp ứng được nhu cầu dựng site tạng bán hàng, thì thử gatsby xem. React hơi rắc rối, nhưng không phải tự nhiên mà giới webdev khắp nơi lại tín nhiệm nó đến thế.