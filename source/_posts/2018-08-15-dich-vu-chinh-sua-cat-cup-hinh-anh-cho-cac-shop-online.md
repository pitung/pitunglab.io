---
title: 'dịch vụ chỉnh sửa, cắt cúp hình ảnh cho các shop online'
type: selling
category: selling
tags: [service, shop, idea]
preview: 'https://aokieudep.com/wp-content/uploads/2018/05/Croptop-kieu-T-shirt.jpg'
overview:
  - đang tưởng tượng
  - triển khai trong năm nay
  - tận dụng thời gian rảnh
  - đang chia gói dịch vụ
  - và ấn định giá cụ thể
  - nhặt tiền lẻ cũng không dễ
  - Hichic...
shipping:
  - chưa sẵn sàng
  - cần người cộng tác
  - nhất là khâu thanh toán
  - và quản lý khách hàng
date: 2018-08-15 23:01:43
summary: Ý tưởng triển khai dịch vụ online, nhặt tiền lẻ cái đã
code: SRV#001
old_price:
price: from 100k
---

Dịch vụ kiểu này mới chỉ là ý tưởng. Tham khảo một số ý kiến thấy có vẻ khó làm. Khó là phải. Việc dễ thiên hạ làm hết cbn rồi.

Anh ý đey

![](https://res.cloudinary.com/bidong/image/upload/v1534349614/pitung.gitlab.io/0812/webpage.jpg)

Luwý đặc biệt tới các cam kết không chia sẻ, không phát tán hình ảnh của khách khi chưa được phép. Cần làm một số hình ảnh cụ thể để khách dễ so sánh và nhận thức được lợi ích do dịch vụ mang lại. Một điểm cũng hơi khó nhằn nữa, là viết tiếng Việt sao cho ngắn gọn và thuyết phục cũng không dễ dàng tí nào.

Đang nghĩ tiếp.

---

Đọc thấy một câu cũng khá thú vị trong [bài này](https://hackernoon.com/key-takeaways-from-startup-accelerator-startupbootcamp-month-one-dea6542cdb64): **"Don’t Look for a Big Idea, Focus on Identifying a Big Problem"**. So, what a big problem with me, now??? I'm seriously.


