---
title: great Art Nouveau painter
type: collection
category: collection
tags:
  - decor
  - art
preview: 'https://res.cloudinary.com/bidong/image/upload/c_scale,w_700/v1532845099/pitung.gitlab.io/0729/maxresdefault.jpg'
summary: my favourite Art Nouveau painter and decorative artist.
artist: alphonse mucha
linkto: 'https://en.wikipedia.org/wiki/Alphonse_Mucha'
date: 2018-07-29 13:24:10
collection:
  - https://res.cloudinary.com/bidong/image/upload/c_scale,w_1000/v1532848681/pitung.gitlab.io/0729/paris-exposition-1900-artist-alphonse-mucha-DDYN18.jpg
  - https://res.cloudinary.com/bidong/image/upload/c_scale,w_1000/v1532845028/pitung.gitlab.io/0729/13a-9in_web1200x1200.jpg
  - https://res.cloudinary.com/bidong/image/upload/c_scale,w_1000/v1532848834/pitung.gitlab.io/0729/csm_Lempertz-943-157-Modern-Art-Alphonse-Mucha-Femme-a-la-Marguerite_148bf10537.jpg
  - https://res.cloudinary.com/bidong/image/upload/c_scale,w_1000/v1532845190/pitung.gitlab.io/0729/005-alphonse-mucha-theredlist.jpg
  - https://res.cloudinary.com/bidong/image/upload/c_scale,w_1000/v1532845099/pitung.gitlab.io/0729/maxresdefault.jpg
  - https://res.cloudinary.com/bidong/image/upload/c_scale,w_1000/v1532848977/pitung.gitlab.io/0729/more-mucha-23.jpg
  - https://res.cloudinary.com/bidong/image/upload/c_scale,w_1000/v1532845147/pitung.gitlab.io/0729/Four_Seasons_by_Alfons_Mucha_circa_1895.jpg
  - https://res.cloudinary.com/bidong/image/upload/c_scale,w_1000/v1532845130/pitung.gitlab.io/0729/nbn8ywv95xzz.jpg

---

Alphonse Mucha was a Czech Art Nouveau painter and decorative artist, known best for his distinct style.