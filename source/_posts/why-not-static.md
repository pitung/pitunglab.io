---
title: why not static?
date: 2017-11-17 11:17:53
tags: [static, webdev]
---

Ta thường chơi lôc wordpress.com. Trướcjờ chưa gặp vấndềji lớn, ngoài việc muốn zùng customized theme hoặc cắmthêm plugin fải jảxèng.

Azzua [fongtrào static site](https://jamstack.org/), ta zựng thử con lôc diêc. Chạy bằng [hexo](https://hexo.io/), host trên [gitlab](http://www.gitlab.com/), deploy qua [netlify](https://www.netlify.com/). Mọithứ chạy ổn, cho dếnkhi ta muốn pôt bài từ con diệnthoại lởm.

Với lôc trên wordpress.com, ta chỉ móc dt ra, viêt email, attach image, gửi bụpfat xong.

Với lôc diêc, ehem... chịu.

Tómlại, khi zùng computer, lôc diêc fuhợp hơn. Nhưng nó chưa dủ tiệnzụng dể zùng trên thiêtbị zidộng.

#### vài zịchvụ nên quantâm

- [Hexo](https://hexo.io/) - javascript base blog framework
- [Gatsby](https://www.gatsbyjs.org/) - fast static site generator (react)
- [Netlify](https://www.netlify.com/) - Automated deployment platform for static site
- [Graphcool](https://www.graph.cool/) and [GraphCMS](https://graphcms.com/) - GraphQL base CMS
- [Firebase](https://firebase.google.com/) - Build apps without managing infrastructure
- [Contentful](https://www.contentful.com/) - content infrastructure
- [Cloudinary](https://cloudinary.com/) - photo and video manipulation

---

### update:

Chán theme cũ, nhảy sang theme khác (cactus-dark). Chạy localhost ok,  push to gitlab ok, gitlab page deploy ok, netlify deploy ok... Mọi thứ OK.

Nhưng site thì biến conbano mât. Déohiểu tại sao.

### jảifap cho netlify?

Déo zùng auto deploy nữa. Mỗi lần xem, sửa các cái trên localhost xong, lệnh

```
hexo generate
```

Xong tóm mẹ cả thưmục public quăng vào netlify cho nó deploy cái thưmục đấy. Coi như upload nguyên cả site một cách thủcông. Kếtqwả site chạy được là ok rồi.

Tạm thế. Lười déo muốn màymò tiếp. Thằng gitlab kia thôi cứ kệ cbn vậy. Có thờizjan sửa lại cái theme cũ rồi deploy sau.