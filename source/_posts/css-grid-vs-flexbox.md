---
title: css grid vs. flexbox
tags: [css, webdev]
refs:
  - title: complet guide to flexbox
    link: https://css-tricks.com/snippets/css/a-guide-to-flexbox/
    from: css-tricks
  - title: complet guide to css grid
    link: https://css-tricks.com/snippets/css/complete-guide-grid/
    from: css-tricks
  - title: css-grid vs. flexbox
    link: https://hackernoon.com/the-ultimate-css-battle-grid-vs-flexbox-d40da0449faf
    from: hackernoon.com
date: 2018-07-31 14:57:40
summary: css grid hay, nhưng không hợp. Flexbox win
---

Tính đến thời điểm này, [gần 85% hỗ trợ grid layout](https://caniuse.com/#search=css-grid) và [gần 95% hỗ trợ flexbox](https://caniuse.com/#search=flexbox). Bày biện research cho sang chảnh, những con số chả ý nghĩa gì mấy. Thực tế là mỗi thằng có cái hay riêng và người ta vẫn có thể dùng lẫn lộn. 

Tùy theo yêu cầu thiết kế mà lựa chọn cho phù hợp thôi.

Trình bày trang web tạng card-based, nếu để bài viết (card) giống hệt nhau nhìn rất nhàm. Giải pháp ưa thích của tao là dùng [masonry](https://masonry.desandro.com/), [isotop](https://isotope.metafizzy.co/), [pakery](https://packery.metafizzy.co/). Trang này không áp dụng, bởi vì cảm thấy không cần thiết.

flex có tính năng `flex-grow` hợp với layout tạng card-based. Khi số lượng card sắp xếp bị lẻ dòng, thì những cái cuối cùng sẽ tự động điều chỉnh chiều rộng của nó để lấp đầy khoảng trống còn lại. Cái này css-grid không xử lý được.

Đây là layout trang web trên desktop

![bidong weblayout](http://res.cloudinary.com/bidong/image/upload/c_scale,w_1000/v1533100670/pitung.gitlab.io/0731/bidong.png)

### tham khảo:

- [css battle - css grid vs flexbox](https://hackernoon.com/the-ultimate-css-battle-grid-vs-flexbox-d40da0449faf)
- [graphic guide to css grid](https://css-tricks.com/snippets/css/complete-guide-grid/)
- [graphic guide to flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)
- [107 mẫu thiết kế content card tuyệt đẹp](https://freefrontend.com/css-cards/)
- [magic of auto margin in flexbox](https://css-tricks.com/the-peculiar-magic-of-flexbox-and-auto-margins/)