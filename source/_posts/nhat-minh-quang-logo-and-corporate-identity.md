---
title: logo and corporate identity
type: selling
category: selling
tags: [design, logo, brand, startup]
preview: 'http://res.cloudinary.com/bidong/image/upload/c_scale,w_700/v1532919472/pitung.gitlab.io/0730/OFS1.jpg'
summary: Gói thiết kế cơ bản dành cho doanh nghiệp startup.
code: 'BRD#001'
price: 5.000k
date: 2018-07-30 09:27:58
overview:
  - all concepts are include
  - high resolution
  - source vector file
  - social media kit
shipping:
  - email, mediafile
  - deliver as payment
  - VAT not included
---

![](https://res.cloudinary.com/bidong/image/upload/v1532919473/pitung.gitlab.io/0730/OFS2.jpg)

## Bạn được gì?

- 3 phương án thiết kế logo, chọn 1 sau tối đa 3 lần review (5 ngày)
- bộ thiết kế giấy tờ văn phòng, bao gồm: namecard, tiêu đề thư (bản in và template cho ứng dụng văn phòng), 2 loại phong bì (2 ngày)
- Giảm giá 15% khi đặt in / gia công 200 sản phẩm (trở lên) mỗi loại

---

![](https://res.cloudinary.com/bidong/image/upload/v1532919472/pitung.gitlab.io/0730/OFS.jpg)

![](https://res.cloudinary.com/bidong/image/upload/v1532919602/pitung.gitlab.io/0730/OFS3.jpg)


## Thảo luận với chúng tôi nếu bạn muốn thêm những lựa chọn khác

- thêm phương án thiết kế
- thêm thời gian, số lần review
- brand guideline (pdf, print)
- thiết kế logo trên các sản phẩm khác
- bao bì, kiểu dáng sản phẩm
- hình ảnh, poster (print, web)
- in ấn, gia công sản phẩm
- thiết kế website