---
title: mouse-over
date: 2018-07-28 15:03:17
tags: [webdev, style, design]
refs:
  - title: card hover animation
    link: https://dev.to/ederchrono/cards-hover-animation---wotw-273f
    from: dev.to
---

Vẫn thích làm mouse over effect cho trang web, nhưng tao bắt đầu thấy trò đó ngớ ngẩn mất thì giờ. Nó chỉ hiệu quả khi chạy trên các thiết bị dùng chuột.

Các hiệu ứng kiểu hiển thị tool-tip, hay bung menu, hay zoom hình ảnh càng hạn chế càng tốt. Về cơ bản, UX cho 2 loại thiết bị đó dựa trên logic khác nhau. Khi nào thiết bị *touch screen* có thể fânbiệt **touch** với **push** trở nên phổ biến, hiệu ứng đó lại có chỗ dùng. Hiện giờ xem trên smartphone + tablet chỉ thấy phiền.

Dùng media querry để hạn chế khi chạy trên màn hình nhỏ cũng được. Nhưng làm mất công quá.