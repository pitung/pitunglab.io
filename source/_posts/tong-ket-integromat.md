---
title: tổng kết integromat
date: 2018-01-03 12:52:44
tags: [integromat, automation, slack]
---

Mới zùng [integromat] 1 tháng, Sốlượng 1000 operation mienfi cạn sạch, chủ yếu zo thửnghiệm chạy đi chạy lại. Lượng zùng thực tế không hết 1/3.

Zùng [slack] một thoizjan thấy tiện hơn email. Ta ýtưởng zùng riêng một private channel làm chỗ input cho các bảng zữliệu. 

---

### why slack

Có fải lúc déonao cũng ngồi máytính dể mở google sheet ra nhập zữliệu dượcdou. Slack thì từ dienthoai dến ipad dến computer dều zùng dược. Mọi nơi, mọi lúc. Tươngtự email, nhưng xem/sửa/xóa zễzàng hơn nhiều.

---

### Zữliệu có ji?

- bảng theozoi [chitieu] hàng ngày
- bảng theozoi [làm khuôn kẽm], zịp cuối năm
- bảng [contact] thường liênhệ

---

Tạm thời tách 3 nhóm thôngtin cần theozoi thành 3 scenario riêng. Thực tế cothe gom lại thành 1 scenario cũng được.

---

### Ghichep [chitieu] như này:

```text
money

chi 400 ăn trưa với khách hàng
thu 5000 bán hàng cho khách
```

Message **fải bătdòu** bằng `money`. Mỗi khoản thu/chi ghi một zòng. `[chi/thu] [số tiền] [môtả]`.

Integromat: B1 -- filter các message bătdòu bằng `money`; B2 -- text parser > match pattern (global, incasesensitive, multiline); B3 -- Lọc zữ liệu chi/thu rồi ghi vào googlsheet.

Regex:

```regex
^(?<action>chi|thu)\s+(?<amount>\d+)\s+(?<desc>.*)$
```

---

### ghichep [làm khuôn kẽm] như này:

```text
khuon

[é]ep 20 10 công ty khongdangky
thuc[k] 10 12 khách hàng sedangky
vm 20 30 khách hàng chuadangky
```

Message **fải bătdòu** bằng `khuon`. Saudo dến `loại khuôn (ep/thuc/vm/cnc...)`. Loại khuôn cothe thêm zóu chấm (`.`) fânbiệt. Vizu: `cnc.bronze, cnc.fip...`

Integromat: B1 -- filter các message bătdòu bằng `khuon`; B2 -- text parser > match pattern (global, incasesensitive, multiline); B3 -- ghi vào google sheet. 

Regex:

```regex
^(?<type>[eé]p|thu[\.a-z]*|cnc[\.a-z]*)\s*(?<w>\d+)[,\s]+(?<h>\d+).(?<desc>.*)$
```

---

### ghichep [contact] fưctạp hơn chút:

Taisao fưctạp: 

- **Vande 1:** slack tựdộng biến email và url thành link khi post message. Vizu: **ten@server.tld** biến thành `<mailto:ten@server.tld|ten@server.tld>`; **google.com** biến thành `<http://google.com|google.com>`.
- **Vande 2:** các trường thongtin của contact khongfai lúc nào cũng dầydủ.
- **Vande 3:** Muốn zùng 1 file google sheet để lưu contact của cả *canhan* và *doanhnghiep* (mỗi cái một sheet). Contact **canhan** baogồm: `name, ext, phone, email`. Contact **doanhnghiep** baogồm: `codename, fullname, website`.
- **Vande 4:** Regex của integromat cove không nhận thamsố `p{L}` (*any unicode character*); [thamkhao](https://www.regular-expressions.info/unicode.html)


Slack message:

```text
canhan

name_ext_more 0949478278 email@server.com (1)
name_ext_more 000 email@anyserver.tld (2)
name_ext_more 012948472924 no@email.com (3)
```

```text
doanhnghiep

codename: tên đầy đủ, website.doanhnghiep.com (1)
codename: noname, website.doanhnghiep.com (2)
codename: tên đầy đủ, noweb.com (3)
```

Contact **canhan** có 3 loại khả năng: (1) contact có đủ tên, số dt, email; (2) không có số dt; (3) không có email. Trườnghợp (2) chỉ cần ghi `zãy số bấtkỳ` *(số chữ số zưới 10)*, trườnghợp (3) ghi dịachỉ email: `no@email.com`.

Tương tự đối với contact **doanhnghiep**. Lưu ý các zóu `:` và `,`. Trườnghợp (2) ghi `noname,`, trườnghợp (3) ghi `noweb.com`.

Regex **canhan**:

```regex
^(?<name>.*?)_(?<ext>.*)\s+(?<phone>\d+).*:(?<email>.+@[^\.].*\.[a-z]{2,})\|
```

Regex **doanhnghiep**:

```regex
^(?<codename>.*)\:\s+(?<fullname>.*),.*?\/.(?<web>[\w\.\/]*)\|
```

---

> **CHÚ Ý:** Nên sửa lại cách ghichep contact canhan và doanhnghiep thành cùng một kiểu cho zễ nhớ. Tao không làm là vì lười thôi.

---

[integromat]: https://www.integromat.com/
[slack]: https://slack.com/
[chitieu]: https://docs.google.com/spreadsheets/d/1-EK1tEH4IeVz9Oo1eix4whx3MbLkSpRZRBDl_lLfDe0/
[làm khuôn kẽm]: https://docs.google.com/spreadsheets/d/1-7BQdDzVe2I_h62YoKGsBMNXydzJiO9UmeLmuyHhkvI/
[contact]: https://docs.google.com/spreadsheets/d/11uDsd4Oy4zevp06hEnh57IktupSn9eG3ZhvAotj84lQ/